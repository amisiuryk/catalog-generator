package com.edunav.catalog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties
@SpringBootApplication(scanBasePackages = "com.edunav.catalog")
public class CatalogGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CatalogGeneratorApplication.class, args);
    }
}
