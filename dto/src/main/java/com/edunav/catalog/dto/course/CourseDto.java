package com.edunav.catalog.dto.course;

import com.edunav.catalog.dto.additional.PrerequisiteSourceTypeDto;
import com.edunav.catalog.dto.requisite.PrerequisiteDto;
import com.edunav.catalog.dto.requisite.Requisite;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public class CourseDto {

    private String id;
    private String aggregatedId;
    private Object changes;
    private List<ActivityInterval> activityIntervals;

    private Date effdtStart;
    private Date effdtEnd;

    private String effectiveTerm;
    private Date effectiveDate;

    private String termStart;
    private String termEnd;
    private boolean active;

    private String displayId;
    private String subject;
    private String courseNumber;
    private String displayCourseNumber;
    private String institutionCourseId;
    private String offer;
    private String description;
    private String name;
    //    @Deprecated
//    private Integer hours;
//    @Deprecated
//    private Float credits;
    private boolean potentiallyContainsPlacements;
    private boolean complexPrerequisites;
    private PrerequisiteDto prerequisites;
    private List<String> corequisites;
    private List<String> attributes;
    private List<CourseAttributeObj> attributeValues;

    private List<String> level;
    private List<DepartmentDto> departments;
    private boolean prerequisitesDiffer;
    private PrerequisiteSourceTypeDto prerequisiteSourceType;
    //    @Deprecated
//    private Integer minCreditHours;
//    @Deprecated
//    private Integer maxCreditHours;
    private Float minCredits;
    private Float maxCredits;
    private String creditRangeMode;
    private Integer repeatLimit;
    private PrerequisiteDto prerequisitesForDebug;
    private RestrictionDto classificationRestriction;
    private RestrictionDto departmentRestriction;
    private RestrictionDto levelRestriction;
    private RestrictionDto majorRestriction;
    private RestrictionDto collegeRestriction;
    private RestrictionDto campusRestriction;
    private RestrictionDto groupRestriction;
    private RestrictionDto studentAttributeRestriction;
    private RestrictionDto catalogCampusRestriction;
    private RestrictionDto cohortRestriction;
    private RestrictionDto degreeRestriction;
    private RestrictionDto programRestriction;
    private RestrictionDto termRestriction;

//    private PrerequisitesDiffDto prerequisitesDiff;
    private List<String> equivalents;
    private List<String> mutuallyExclusive;
    private String year;
    private Requisite requisite;
    private String creditStatus;
    private Boolean noncredit;
    private Boolean degreeApplicable;
    private String typicallyOffered;

    private String processingLog;
    private Integer maximumRepeatCredits;
    private Map<String, Object> debugData;

    private String college;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAggregatedId() {
        return aggregatedId;
    }

    public void setAggregatedId(String aggregatedId) {
        this.aggregatedId = aggregatedId;
    }

    public Object getChanges() {
        return changes;
    }

    public void setChanges(Object changes) {
        this.changes = changes;
    }

    public List<ActivityInterval> getActivityIntervals() {
        return activityIntervals;
    }

    public void setActivityIntervals(List<ActivityInterval> activityIntervals) {
        this.activityIntervals = activityIntervals;
    }

    public Date getEffdtStart() {
        return effdtStart;
    }

    public void setEffdtStart(Date effdtStart) {
        this.effdtStart = effdtStart;
    }

    public Date getEffdtEnd() {
        return effdtEnd;
    }

    public void setEffdtEnd(Date effdtEnd) {
        this.effdtEnd = effdtEnd;
    }

    public String getEffectiveTerm() {
        return effectiveTerm;
    }

    public void setEffectiveTerm(String effectiveTerm) {
        this.effectiveTerm = effectiveTerm;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public String getTermStart() {
        return termStart;
    }

    public void setTermStart(String termStart) {
        this.termStart = termStart;
    }

    public String getTermEnd() {
        return termEnd;
    }

    public void setTermEnd(String termEnd) {
        this.termEnd = termEnd;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCourseNumber() {
        return courseNumber;
    }

    public void setCourseNumber(String courseNumber) {
        this.courseNumber = courseNumber;
    }

    public String getDisplayCourseNumber() {
        return displayCourseNumber;
    }

    public void setDisplayCourseNumber(String displayCourseNumber) {
        this.displayCourseNumber = displayCourseNumber;
    }

    public String getInstitutionCourseId() {
        return institutionCourseId;
    }

    public void setInstitutionCourseId(String institutionCourseId) {
        this.institutionCourseId = institutionCourseId;
    }

    public String getOffer() {
        return offer;
    }

    public void setOffer(String offer) {
        this.offer = offer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPotentiallyContainsPlacements() {
        return potentiallyContainsPlacements;
    }

    public void setPotentiallyContainsPlacements(boolean potentiallyContainsPlacements) {
        this.potentiallyContainsPlacements = potentiallyContainsPlacements;
    }

    public boolean isComplexPrerequisites() {
        return complexPrerequisites;
    }

    public void setComplexPrerequisites(boolean complexPrerequisites) {
        this.complexPrerequisites = complexPrerequisites;
    }

    public PrerequisiteDto getPrerequisites() {
        return prerequisites;
    }

    public void setPrerequisites(PrerequisiteDto prerequisites) {
        this.prerequisites = prerequisites;
    }

    public List<String> getCorequisites() {
        return corequisites;
    }

    public void setCorequisites(List<String> corequisites) {
        this.corequisites = corequisites;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }

    public List<CourseAttributeObj> getAttributeValues() {
        return attributeValues;
    }

    public void setAttributeValues(List<CourseAttributeObj> attributeValues) {
        this.attributeValues = attributeValues;
    }

    public List<String> getLevel() {
        return level;
    }

    public void setLevel(List<String> level) {
        this.level = level;
    }

    public List<DepartmentDto> getDepartments() {
        return departments;
    }

    public void setDepartments(List<DepartmentDto> departments) {
        this.departments = departments;
    }

    public boolean isPrerequisitesDiffer() {
        return prerequisitesDiffer;
    }

    public void setPrerequisitesDiffer(boolean prerequisitesDiffer) {
        this.prerequisitesDiffer = prerequisitesDiffer;
    }

    public Float getMinCredits() {
        return minCredits;
    }

    public void setMinCredits(Float minCredits) {
        this.minCredits = minCredits;
    }

    public Float getMaxCredits() {
        return maxCredits;
    }

    public void setMaxCredits(Float maxCredits) {
        this.maxCredits = maxCredits;
    }

    public String getCreditRangeMode() {
        return creditRangeMode;
    }

    public void setCreditRangeMode(String creditRangeMode) {
        this.creditRangeMode = creditRangeMode;
    }

    public Integer getRepeatLimit() {
        return repeatLimit;
    }

    public void setRepeatLimit(Integer repeatLimit) {
        this.repeatLimit = repeatLimit;
    }

    public PrerequisiteDto getPrerequisitesForDebug() {
        return prerequisitesForDebug;
    }

    public void setPrerequisitesForDebug(PrerequisiteDto prerequisitesForDebug) {
        this.prerequisitesForDebug = prerequisitesForDebug;
    }

    public RestrictionDto getClassificationRestriction() {
        return classificationRestriction;
    }

    public void setClassificationRestriction(RestrictionDto classificationRestriction) {
        this.classificationRestriction = classificationRestriction;
    }

    public RestrictionDto getDepartmentRestriction() {
        return departmentRestriction;
    }

    public void setDepartmentRestriction(RestrictionDto departmentRestriction) {
        this.departmentRestriction = departmentRestriction;
    }

    public RestrictionDto getLevelRestriction() {
        return levelRestriction;
    }

    public void setLevelRestriction(RestrictionDto levelRestriction) {
        this.levelRestriction = levelRestriction;
    }

    public RestrictionDto getMajorRestriction() {
        return majorRestriction;
    }

    public void setMajorRestriction(RestrictionDto majorRestriction) {
        this.majorRestriction = majorRestriction;
    }

    public RestrictionDto getCollegeRestriction() {
        return collegeRestriction;
    }

    public void setCollegeRestriction(RestrictionDto collegeRestriction) {
        this.collegeRestriction = collegeRestriction;
    }

    public RestrictionDto getCampusRestriction() {
        return campusRestriction;
    }

    public void setCampusRestriction(RestrictionDto campusRestriction) {
        this.campusRestriction = campusRestriction;
    }

    public RestrictionDto getGroupRestriction() {
        return groupRestriction;
    }

    public void setGroupRestriction(RestrictionDto groupRestriction) {
        this.groupRestriction = groupRestriction;
    }

    public RestrictionDto getStudentAttributeRestriction() {
        return studentAttributeRestriction;
    }

    public void setStudentAttributeRestriction(RestrictionDto studentAttributeRestriction) {
        this.studentAttributeRestriction = studentAttributeRestriction;
    }

    public RestrictionDto getCatalogCampusRestriction() {
        return catalogCampusRestriction;
    }

    public void setCatalogCampusRestriction(RestrictionDto catalogCampusRestriction) {
        this.catalogCampusRestriction = catalogCampusRestriction;
    }

    public RestrictionDto getCohortRestriction() {
        return cohortRestriction;
    }

    public void setCohortRestriction(RestrictionDto cohortRestriction) {
        this.cohortRestriction = cohortRestriction;
    }

    public RestrictionDto getDegreeRestriction() {
        return degreeRestriction;
    }

    public void setDegreeRestriction(RestrictionDto degreeRestriction) {
        this.degreeRestriction = degreeRestriction;
    }

    public RestrictionDto getProgramRestriction() {
        return programRestriction;
    }

    public void setProgramRestriction(RestrictionDto programRestriction) {
        this.programRestriction = programRestriction;
    }

    public RestrictionDto getTermRestriction() {
        return termRestriction;
    }

    public void setTermRestriction(RestrictionDto termRestriction) {
        this.termRestriction = termRestriction;
    }

    public List<String> getEquivalents() {
        return equivalents;
    }

    public void setEquivalents(List<String> equivalents) {
        this.equivalents = equivalents;
    }

    public List<String> getMutuallyExclusive() {
        return mutuallyExclusive;
    }

    public void setMutuallyExclusive(List<String> mutuallyExclusive) {
        this.mutuallyExclusive = mutuallyExclusive;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Requisite getRequisite() {
        return requisite;
    }

    public void setRequisite(Requisite requisite) {
        this.requisite = requisite;
    }

    public String getCreditStatus() {
        return creditStatus;
    }

    public void setCreditStatus(String creditStatus) {
        this.creditStatus = creditStatus;
    }

    public Boolean getNoncredit() {
        return noncredit;
    }

    public void setNoncredit(Boolean noncredit) {
        this.noncredit = noncredit;
    }

    public Boolean getDegreeApplicable() {
        return degreeApplicable;
    }

    public void setDegreeApplicable(Boolean degreeApplicable) {
        this.degreeApplicable = degreeApplicable;
    }

    public String getTypicallyOffered() {
        return typicallyOffered;
    }

    public void setTypicallyOffered(String typicallyOffered) {
        this.typicallyOffered = typicallyOffered;
    }

    public String getProcessingLog() {
        return processingLog;
    }

    public void setProcessingLog(String processingLog) {
        this.processingLog = processingLog;
    }

    public Integer getMaximumRepeatCredits() {
        return maximumRepeatCredits;
    }

    public void setMaximumRepeatCredits(Integer maximumRepeatCredits) {
        this.maximumRepeatCredits = maximumRepeatCredits;
    }

    public Map<String, Object> getDebugData() {
        return debugData;
    }

    public void setDebugData(Map<String, Object> debugData) {
        this.debugData = debugData;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public PrerequisiteSourceTypeDto getPrerequisiteSourceType() {
        return prerequisiteSourceType;
    }

    public void setPrerequisiteSourceType(PrerequisiteSourceTypeDto prerequisiteSourceType) {
        this.prerequisiteSourceType = prerequisiteSourceType;
    }
}
