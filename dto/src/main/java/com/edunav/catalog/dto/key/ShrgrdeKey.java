package com.edunav.catalog.dto.key;

import java.io.Serializable;

public class ShrgrdeKey implements Serializable {

    private String code;
    private String levlCode;
    private String termCodeEffective;

    public ShrgrdeKey(String code, String levlCode, String termCodeEffective) {
        this.code = code;
        this.levlCode = levlCode;
        this.termCodeEffective = termCodeEffective;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevlCode() {
        return levlCode;
    }

    public void setLevlCode(String levlCode) {
        this.levlCode = levlCode;
    }

    public String getTermCodeEffective() {
        return termCodeEffective;
    }

    public void setTermCodeEffective(String termCodeEffective) {
        this.termCodeEffective = termCodeEffective;
    }
}
