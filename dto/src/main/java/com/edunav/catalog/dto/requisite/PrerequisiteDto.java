package com.edunav.catalog.dto.requisite;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * Created by kip on 3/11/15.
 */
public class PrerequisiteDto {

    private List<Object> and;
    private List<Object> or;
    private Double requiredNumberOfConditions;
    private String areaCode;
    private String ruleCode;
    private String groupCode;
    private String connectorCond;
    private String connectorMaxCond;
    private String connectorReq;
    private String connectorMax;
    private Integer complCourses;
    private Double complCredits;
    private Integer maxCourses;
    private Integer maxCoursesTransfer;
    private Double maxCredits;
    private Double maxCreditsTransfer;
    private Double maxCredCrse;
    private Double minCredCrse;
    private Integer reqCourses;
    private Double reqCredits;
    private Double maxCredCond;
    private Integer reqCoursesCond;
    private Double reqCredCond;
    private String connectorTransfer;
    private Integer numberOfYears;
    private String fromTerm;
    private String toTerm;
    private String campCode;
    private String collCode;
    private String deptCode;

    public static PrerequisiteDto buildOr() {
        PrerequisiteDto prerequisite = new PrerequisiteDto();
        prerequisite.setOr(new ArrayList<>());
        return prerequisite;
    }

    public static PrerequisiteDto buildAnd(Object... elements) {
        PrerequisiteDto prerequisite = new PrerequisiteDto();
        ArrayList<Object> and = new ArrayList<>();
        prerequisite.setAnd(and);
        for (Object element : elements) {
            and.add(element);
        }
        return prerequisite;
    }

    public List<Object> getAnd() {
        return and;
    }

    public void setAnd(List<Object> and) {
        this.and = and;
    }

    public List<Object> getOr() {
        return or;
    }

    public void setOr(List<Object> or) {
        this.or = or;
    }

    @JsonIgnore
    public List<Object> getList() {
        if (and == null) {
            return or;
        }
        return and;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        if (nonNull(and)) {
            s.append("and: ");
            s.append(and + "\n");
        }
        if (nonNull(or)) {
            s.append("or: ");
            s.append("\n" + or);
        }
        return s.toString();
    }

    @JsonIgnore
    public boolean isOrCondition() {
        return or != null;
    }

    public Double getRequiredNumberOfConditions() {
        return requiredNumberOfConditions;
    }

    public void setRequiredNumberOfConditions(Double requiredNumberOfConditions) {
        this.requiredNumberOfConditions = requiredNumberOfConditions;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getConnectorCond() {
        return connectorCond;
    }

    public void setConnectorCond(String connectorCond) {
        this.connectorCond = connectorCond;
    }

    public String getConnectorMaxCond() {
        return connectorMaxCond;
    }

    public void setConnectorMaxCond(String connectorMaxCond) {
        this.connectorMaxCond = connectorMaxCond;
    }

    public String getConnectorReq() {
        return connectorReq;
    }

    public void setConnectorReq(String connectorReq) {
        this.connectorReq = connectorReq;
    }

    public String getConnectorMax() {
        return connectorMax;
    }

    public void setConnectorMax(String connectorMax) {
        this.connectorMax = connectorMax;
    }

    public Integer getComplCourses() {
        return complCourses;
    }

    public void setComplCourses(Integer complCourses) {
        this.complCourses = complCourses;
    }

    public Double getComplCredits() {
        return complCredits;
    }

    public void setComplCredits(Double complCredits) {
        this.complCredits = complCredits;
    }

    public Integer getMaxCourses() {
        return maxCourses;
    }

    public void setMaxCourses(Integer maxCourses) {
        this.maxCourses = maxCourses;
    }

    public Integer getMaxCoursesTransfer() {
        return maxCoursesTransfer;
    }

    public void setMaxCoursesTransfer(Integer maxCoursesTransfer) {
        this.maxCoursesTransfer = maxCoursesTransfer;
    }

    public Double getMaxCredits() {
        return maxCredits;
    }

    public void setMaxCredits(Double maxCredits) {
        this.maxCredits = maxCredits;
    }

    public Double getMaxCreditsTransfer() {
        return maxCreditsTransfer;
    }

    public void setMaxCreditsTransfer(Double maxCreditsTransfer) {
        this.maxCreditsTransfer = maxCreditsTransfer;
    }

    public Double getMaxCredCrse() {
        return maxCredCrse;
    }

    public void setMaxCredCrse(Double maxCredCrse) {
        this.maxCredCrse = maxCredCrse;
    }

    public Double getMinCredCrse() {
        return minCredCrse;
    }

    public void setMinCredCrse(Double minCredCrse) {
        this.minCredCrse = minCredCrse;
    }

    public Integer getReqCourses() {
        return reqCourses;
    }

    public void setReqCourses(Integer reqCourses) {
        this.reqCourses = reqCourses;
    }

    public Double getReqCredits() {
        return reqCredits;
    }

    public void setReqCredits(Double reqCredits) {
        this.reqCredits = reqCredits;
    }

    public Double getMaxCredCond() {
        return maxCredCond;
    }

    public void setMaxCredCond(Double maxCredCond) {
        this.maxCredCond = maxCredCond;
    }

    public Integer getReqCoursesCond() {
        return reqCoursesCond;
    }

    public void setReqCoursesCond(Integer reqCoursesCond) {
        this.reqCoursesCond = reqCoursesCond;
    }

    public Double getReqCredCond() {
        return reqCredCond;
    }

    public void setReqCredCond(Double reqCredCond) {
        this.reqCredCond = reqCredCond;
    }

    public String getConnectorTransfer() {
        return connectorTransfer;
    }

    public void setConnectorTransfer(String connectorTransfer) {
        this.connectorTransfer = connectorTransfer;
    }

    public Integer getNumberOfYears() {
        return numberOfYears;
    }

    public void setNumberOfYears(Integer numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    public String getFromTerm() {
        return fromTerm;
    }

    public void setFromTerm(String fromTerm) {
        this.fromTerm = fromTerm;
    }

    public String getToTerm() {
        return toTerm;
    }

    public void setToTerm(String toTerm) {
        this.toTerm = toTerm;
    }

    public String getCampCode() {
        return campCode;
    }

    public void setCampCode(String campCode) {
        this.campCode = campCode;
    }

    public String getCollCode() {
        return collCode;
    }

    public void setCollCode(String collCode) {
        this.collCode = collCode;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }
}
