package com.edunav.catalog.dto.requisite;

import java.util.List;

public class MinGradeSet {
    private Double minimumNumericalGrade;
    List<String> passingSpecialGrades;

    public Double getMinimumNumericalGrade() {
        return minimumNumericalGrade;
    }

    public void setMinimumNumericalGrade(Double minimumNumericalGrade) {
        this.minimumNumericalGrade = minimumNumericalGrade;
    }

    public List<String> getPassingSpecialGrades() {
        return passingSpecialGrades;
    }

    public void setPassingSpecialGrades(List<String> passingSpecialGrades) {
        this.passingSpecialGrades = passingSpecialGrades;
    }
}
