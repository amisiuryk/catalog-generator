package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScrtextKey implements Serializable {

    private Integer id;

    public ScrtextKey(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScrtextKey)) return false;
        ScrtextKey that = (ScrtextKey) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ScrtextKey{" +
                "id='" + id + '\'' +
                '}';
    }
}
