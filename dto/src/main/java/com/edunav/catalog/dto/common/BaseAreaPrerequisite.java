package com.edunav.catalog.dto.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class BaseAreaPrerequisite implements Comparable {

    private String id;
    private String prerequisitesCourseId;
    private String keyRule;
    private String keyGroupArea;
    private String prerequisitesType;
    private String test;
    private String testScore;
    private String maxTestScore;
    private String courseAttrCode;
    private String studentAttrCode;

    private double prerequisiteSeqno;
    private String leftParen;
    private String rightParent;
    private boolean concurrency;
    private String grade;
    private String set;
    private String subSet;
    private String area;
    private String rule;
    private String group;
    private CourseRange range;
    private Double requiredNumberOfConditions;
    private Double numberOfYears;
    private String fromTerm;
    private String toTerm;
    private String effectiveTerm;
    private String courseId;

    public void setPrerequisiteRelation(String prerequisitesType) {
        this.prerequisitesType = prerequisitesType;
    }

    public String getPrerequisitesType() {
        return prerequisitesType;
    }

    public void setPrerequisitesType(String prerequisitesType) {
        this.prerequisitesType = prerequisitesType;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getTestScore() {
        return testScore;
    }

    public void setTestScore(String testScore) {
        this.testScore = testScore;
    }

    public String getMaxTestScore() {
        return maxTestScore;
    }

    public void setMaxTestScore(String maxTestScore) {
        this.maxTestScore = maxTestScore;
    }

    public double getPrerequisiteSeqno() {
        return prerequisiteSeqno;
    }

    public void setPrerequisiteSeqno(double prerequisiteSeqno) {
        this.prerequisiteSeqno = prerequisiteSeqno;
    }

    public String getLeftParen() {
        return leftParen;
    }

    public void setLeftParen(String leftParen) {
        this.leftParen = leftParen;
    }

    public String getRightParent() {
        return rightParent;
    }

    public void setRightParent(String rightParent) {
        this.rightParent = rightParent;
    }

    public boolean getConcurrency() {
        return concurrency;
    }

    public void setConcurrency(boolean concurrency) {
        this.concurrency = concurrency;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAreaKeyRule() {
        return StringUtils.defaultString(getArea()).concat(StringUtils.defaultString(getKeyRule()));
    }

    public String getAreaRule() {
        return StringUtils.defaultString(getArea()).concat(StringUtils.defaultString(getRule()));
    }

    public String getKeyRule() {
        return keyRule;
    }

    public void setKeyRule(String keyRule) {
        this.keyRule = keyRule;
    }

    public String getKeyGroupArea() {
        return keyGroupArea;
    }

    public void setKeyGroupArea(String keyGroupArea) {
        this.keyGroupArea = keyGroupArea;
    }

    public boolean isConcurrency() {
        return concurrency;
    }

    public String getSet() {

        return StringUtils.trimToEmpty(StringUtils.defaultString(set));
    }

    public void setSet(String set) {
        this.set = set;
    }

    public String getSubSet() {
        return StringUtils.defaultString(subSet);
    }

    public void setSubSet(String subSet) {
        this.subSet = subSet;
    }

    public String getArea() {
        return StringUtils.defaultString(area);
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getRule() {
        return StringUtils.defaultString(rule);
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getGroup() {
        return StringUtils.defaultString(group);
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseAreaPrerequisite)) return false;
        BaseAreaPrerequisite that = (BaseAreaPrerequisite) o;
        return Double.compare(that.prerequisiteSeqno, prerequisiteSeqno) == 0 && concurrency == that.concurrency && Objects.equals(id, that.id) && Objects.equals(prerequisitesCourseId, that.prerequisitesCourseId) && Objects.equals(keyRule, that.keyRule) && Objects.equals(keyGroupArea, that.keyGroupArea) && Objects.equals(prerequisitesType, that.prerequisitesType) && Objects.equals(test, that.test) && Objects.equals(testScore, that.testScore) && Objects.equals(maxTestScore, that.maxTestScore) && Objects.equals(courseAttrCode, that.courseAttrCode) && Objects.equals(studentAttrCode, that.studentAttrCode) && Objects.equals(leftParen, that.leftParen) && Objects.equals(rightParent, that.rightParent) && Objects.equals(grade, that.grade) && Objects.equals(set, that.set) && Objects.equals(subSet, that.subSet) && Objects.equals(area, that.area) && Objects.equals(rule, that.rule) && Objects.equals(group, that.group) && Objects.equals(range, that.range) && Objects.equals(requiredNumberOfConditions, that.requiredNumberOfConditions) && Objects.equals(numberOfYears, that.numberOfYears) && Objects.equals(fromTerm, that.fromTerm) && Objects.equals(toTerm, that.toTerm) && Objects.equals(effectiveTerm, that.effectiveTerm) && Objects.equals(courseId, that.courseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, prerequisitesCourseId, keyRule, keyGroupArea, prerequisitesType, test, testScore, maxTestScore, courseAttrCode, studentAttrCode, prerequisiteSeqno, leftParen, rightParent, concurrency, grade, set, subSet, area, rule, group, range, requiredNumberOfConditions, numberOfYears, fromTerm, toTerm, effectiveTerm, courseId);
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o))
            return 0;
        else
            return this.getPrerequisiteSeqno() > ((BaseAreaPrerequisite) o).getPrerequisiteSeqno() ? 1 : -1;

    }

    public String getCourseAttrCode() {
        return courseAttrCode;
    }

    public void setCourseAttrCode(String courseAttrCode) {
        this.courseAttrCode = courseAttrCode;
    }

    public String getStudentAttrCode() {
        return studentAttrCode;
    }

    public void setStudentAttrCode(String studentAttrCode) {
        this.studentAttrCode = studentAttrCode;
    }

    public CourseRange getRange() {
        return range;
    }

    public void setRange(CourseRange range) {
        this.range = range;
    }

    public Double getRequiredNumberOfConditions() {
        return requiredNumberOfConditions;
    }

    public void setRequiredNumberOfConditions(Double requiredNumberOfConditions) {
        this.requiredNumberOfConditions = requiredNumberOfConditions;
    }

    public String getPrerequisitesCourseId() {
        return prerequisitesCourseId;
    }

    public void setPrerequisitesCourseId(String prerequisitesCourseId) {
        this.prerequisitesCourseId = prerequisitesCourseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getNumberOfYears() {
        return numberOfYears;
    }

    public void setNumberOfYears(Double numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    public String getFromTerm() {
        return fromTerm;
    }

    public void setFromTerm(String fromTerm) {
        this.fromTerm = fromTerm;
    }

    public String getToTerm() {
        return toTerm;
    }

    public void setToTerm(String toTerm) {
        this.toTerm = toTerm;
    }

    public String getEffectiveTerm() {
        return effectiveTerm;
    }

    public void setEffectiveTerm(String effectiveTerm) {
        this.effectiveTerm = effectiveTerm;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
}
