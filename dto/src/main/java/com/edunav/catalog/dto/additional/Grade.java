package com.edunav.catalog.dto.additional;

public class Grade {
    private String id;
    private String grade;
    private String abbrev;
    private String level;
    private String term;
    private Double qualityPoints;
    private Boolean isAttempted;
    private Boolean isCompleted;
    private Boolean isPassed;
    private Boolean isGpa;
    private Integer value;
    private Boolean isInProgress;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Boolean getIsPassed() {
        return isPassed;
    }

    public void setIsPassed(Boolean isPassed) {
        this.isPassed = isPassed;
    }

    public Boolean getIsGpa() {
        return isGpa;
    }

    public void setIsGpa(Boolean isGpa) {
        this.isGpa = isGpa;
    }

    public Boolean getIsAttempted() {
        return isAttempted;
    }

    public void setIsAttempted(Boolean isAttempted) {
        this.isAttempted = isAttempted;
    }

    public Boolean getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(Boolean isCompleted) {
        this.isCompleted = isCompleted;
    }

    public Double getQualityPoints() {
        return qualityPoints;
    }

    public void setQualityPoints(Double qualityPoints) {
        this.qualityPoints = qualityPoints;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getAbbrev() {
        return abbrev;
    }

    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "GradeModel{" +
                "grade='" + grade + '\'' +
                ", abbrev='" + abbrev + '\'' +
                ", level='" + level + '\'' +
                ", term='" + term + '\'' +
                ", qualityPoints=" + qualityPoints +
                ", isAttempted=" + isAttempted +
                ", isCompleted=" + isCompleted +
                ", isPassed=" + isPassed +
                ", isGpa=" + isGpa +
                ", value=" + value +
                ", isInProgress=" + isInProgress +
                '}';
    }

    public Boolean getIsInProgress() {
        return isInProgress;
    }

    public void setIsInProgress(Boolean inProgress) {
        isInProgress = inProgress;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
