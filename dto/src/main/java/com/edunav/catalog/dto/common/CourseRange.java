package com.edunav.catalog.dto.common;

import java.util.Objects;

public class CourseRange {
    private String subject;
    private String from;
    private String to;

    public CourseRange() {
    }

    public CourseRange(String subject, String from, String to) {
        this.subject = subject;
        this.from = from;
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CourseRange that = (CourseRange) o;
        return Objects.equals(subject, that.subject) &&
                Objects.equals(from, that.from) &&
                Objects.equals(to, that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subject, from, to);
    }
}
