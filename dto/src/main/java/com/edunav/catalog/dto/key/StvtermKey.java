package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class StvtermKey implements Serializable {

    private String code;

    public StvtermKey(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
