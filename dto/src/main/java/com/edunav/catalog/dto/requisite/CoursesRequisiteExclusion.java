package com.edunav.catalog.dto.requisite;

import com.edunav.catalog.dto.course.DepartmentDto;

import java.util.List;

public class CoursesRequisiteExclusion {
    private String id;
    private String subject;
    private String campus;
    private String college;
    private List<DepartmentDto> departments;
    private String termStart;
    private String termEnd;
    private List<String> attributes;
    private String studentAttrCode;

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getTermStart() {
        return termStart;
    }

    public void setTermStart(String termStart) {
        this.termStart = termStart;
    }

    public String getTermEnd() {
        return termEnd;
    }

    public void setTermEnd(String termEnd) {
        this.termEnd = termEnd;
    }

    public String getStudentAttrCode() {
        return studentAttrCode;
    }

    public void setStudentAttrCode(String studentAttrCode) {
        this.studentAttrCode = studentAttrCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<DepartmentDto> getDepartments() {
        return departments;
    }

    public void setDepartments(List<DepartmentDto> departments) {
        this.departments = departments;
    }

    public List<String> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<String> attributes) {
        this.attributes = attributes;
    }
}