package com.edunav.catalog.dto.prerequisite;

import com.edunav.catalog.dto.common.CourseRange;
import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public class PrerequisiteExclusionModel implements Comparable {
    private String parentId;
    private String area;
    private String keyRule;
    private String keyGroupArea;
    private String prerequisitesCourseId;
    private String prerequisitesSubject;
    private double prerequisiteSeqno;
    private String courseAttrCode;
    private String studentAttrCode;
    private String campus;
    private String college;
    private String department;
    private String termStart;
    private String termEnd;
    private String term;
    private CourseRange range;
    private String effectiveTerm;
    private String courseId;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getAreaKeyRule() {
        return StringUtils.defaultString(getArea()) + StringUtils.defaultString(getKeyRule());
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getKeyRule() {
        return keyRule;
    }

    public void setKeyRule(String keyRule) {
        this.keyRule = keyRule;
    }

    public double getPrerequisiteSeqno() {
        return prerequisiteSeqno;
    }

    public void setPrerequisiteSeqno(double prerequisiteSeqno) {
        this.prerequisiteSeqno = prerequisiteSeqno;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTermStart() {
        return termStart;
    }

    public void setTermStart(String termStart) {
        this.termStart = termStart;
    }

    public String getTermEnd() {
        return termEnd;
    }

    public void setTermEnd(String termEnd) {
        this.termEnd = termEnd;
    }

    public String getKeyGroupArea() {
        return keyGroupArea;
    }

    public void setKeyGroupArea(String keyGroupArea) {
        this.keyGroupArea = keyGroupArea;
    }

    public String getPrerequisitesCourseId() {
        return prerequisitesCourseId;
    }

    public void setPrerequisitesCourseId(String prerequisitesCourseId) {
        this.prerequisitesCourseId = prerequisitesCourseId;
    }

    public String getCourseAttrCode() {
        return courseAttrCode;
    }

    public void setCourseAttrCode(String courseAttrCode) {
        this.courseAttrCode = courseAttrCode;
    }

    public String getStudentAttrCode() {
        return studentAttrCode;
    }

    public void setStudentAttrCode(String studentAttrCode) {
        this.studentAttrCode = studentAttrCode;
    }

    public CourseRange getRange() {
        return range;
    }

    public void setRange(CourseRange range) {
        this.range = range;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getEffectiveTerm() {
        return effectiveTerm;
    }

    public void setEffectiveTerm(String effectiveTerm) {
        this.effectiveTerm = effectiveTerm;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PrerequisiteExclusionModel that = (PrerequisiteExclusionModel) o;
        return Double.compare(that.prerequisiteSeqno, prerequisiteSeqno) == 0 &&
//                Objects.equals(parentId, that.parentId) &&
                Objects.equals(area, that.area) &&
                Objects.equals(keyRule, that.keyRule) &&
                Objects.equals(keyGroupArea, that.keyGroupArea) &&
                Objects.equals(courseAttrCode, that.courseAttrCode) &&
                Objects.equals(studentAttrCode, that.studentAttrCode) &&
                Objects.equals(campus, that.campus) &&
                Objects.equals(college, that.college) &&
                Objects.equals(department, that.department) &&
                Objects.equals(termStart, that.termStart) &&
                Objects.equals(termEnd, that.termEnd) &&
                Objects.equals(term, that.term) &&
                Objects.equals(prerequisitesCourseId, that.prerequisitesCourseId) &&
                Objects.equals(prerequisitesSubject, that.prerequisitesSubject) &&
                Objects.equals(range, that.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prerequisitesCourseId, area, keyRule, keyGroupArea, prerequisitesSubject, courseAttrCode,
                studentAttrCode, campus, college, department, termStart, termEnd, range, term);
    }

    @Override
    public int compareTo(Object o) {
        if (this.equals(o))
            return 0;
        else
            return this.getPrerequisiteSeqno() > ((PrerequisiteExclusionModel) o).getPrerequisiteSeqno() ? 1 : -1;
    }

    public String getPrerequisitesSubject() {
        return prerequisitesSubject;
    }

    public void setPrerequisitesSubject(String prerequisitesSubject) {
        this.prerequisitesSubject = prerequisitesSubject;
    }
}

