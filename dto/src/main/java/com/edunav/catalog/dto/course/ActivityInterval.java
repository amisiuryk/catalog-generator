package com.edunav.catalog.dto.course;

import java.io.Serializable;
import java.util.Date;

public class ActivityInterval implements Serializable {
    private Date start;
    private Date end;
    private String termStart;
    private String termEnd;
    private boolean active;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }


    public String getTermStart() {
        return termStart;
    }

    public void setTermStart(String termStart) {
        this.termStart = termStart;
    }

    public String getTermEnd() {
        return termEnd;
    }

    public void setTermEnd(String termEnd) {
        this.termEnd = termEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivityInterval that = (ActivityInterval) o;

        if (active != that.active) return false;
        if (start != null ? !start.equals(that.start) : that.start != null) return false;
        if (end != null ? !end.equals(that.end) : that.end != null) return false;
        if (termStart != null ? !termStart.equals(that.termStart) : that.termStart != null) return false;
        return termEnd != null ? termEnd.equals(that.termEnd) : that.termEnd == null;
    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (termStart != null ? termStart.hashCode() : 0);
        result = 31 * result + (termEnd != null ? termEnd.hashCode() : 0);
        result = 31 * result + (active ? 1 : 0);
        return result;
    }
    public ActivityInterval() {

    }

    public ActivityInterval(Date start, Date end, String termStart, String termEnd, boolean active) {
        this.start = start;
        this.end = end;
        this.termStart = termStart;
        this.termEnd = termEnd;
        this.active = active;
    }

    public ActivityInterval(ActivityInterval other) {
        this.start = other.start;
        this.end = other.end;
        this.termStart = other.termStart;
        this.termEnd = other.termEnd;
        this.active = other.active;
    }

    public ActivityInterval withStart(final Date start) {
        this.start = start;
        return this;
    }

    public ActivityInterval withEnd(final Date end) {
        this.end = end;
        return this;
    }

    public ActivityInterval withTermStart(final String termStart) {
        this.termStart = termStart;
        return this;
    }

    public ActivityInterval withTermEnd(final String termEnd) {
        this.termEnd = termEnd;
        return this;
    }

    public ActivityInterval withActive(final boolean active) {
        this.active = active;
        return this;
    }


}
