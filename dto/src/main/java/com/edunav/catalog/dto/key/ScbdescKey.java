package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScbdescKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String termCodeEff;
    private String termCodeEnd;

    public ScbdescKey(String subjCode, String crseNumb, String termCodeEff, String termCodeEnd) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.termCodeEff = termCodeEff;
        this.termCodeEnd = termCodeEnd;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getTermCodeEff() {
        return termCodeEff;
    }

    public void setTermCodeEff(String termCodeEff) {
        this.termCodeEff = termCodeEff;
    }

    public String getTermCodeEnd() {
        return termCodeEnd;
    }

    public void setTermCodeEnd(String termCodeEnd) {
        this.termCodeEnd = termCodeEnd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScbdescKey)) return false;
        ScbdescKey that = (ScbdescKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(termCodeEff, that.termCodeEff) && Objects.equals(termCodeEnd, that.termCodeEnd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, termCodeEff, termCodeEnd);
    }

    @Override
    public String toString() {
        return "ScbdescKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", termCodeEff='" + termCodeEff + '\'' +
                ", termCodeEnd='" + termCodeEnd + '\'' +
                '}';
    }
}
