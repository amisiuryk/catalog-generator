package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScbcrseKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String effTerm;

    public ScbcrseKey(String subjCode, String crseNumb, String effTerm) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.effTerm = effTerm;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(String effTerm) {
        this.effTerm = effTerm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScbcrseKey)) return false;
        ScbcrseKey that = (ScbcrseKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(effTerm, that.effTerm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, effTerm);
    }

    @Override
    public String toString() {
        return "ScbcrseKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", effTerm='" + effTerm + '\'' +
                '}';
    }
}
