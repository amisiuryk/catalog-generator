package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScreqivKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String effTerm;
    private String subjCodeEqiv;
    private String crseNumbEqiv;
    private String startTerm;

    public ScreqivKey(String subjCode, String crseNumb, String effTerm, String subjCodeEqiv, String crseNumbEqiv, String startTerm) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.effTerm = effTerm;
        this.subjCodeEqiv = subjCodeEqiv;
        this.crseNumbEqiv = crseNumbEqiv;
        this.startTerm = startTerm;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(String effTerm) {
        this.effTerm = effTerm;
    }

    public String getSubjCodeEqiv() {
        return subjCodeEqiv;
    }

    public void setSubjCodeEqiv(String subjCodeEqiv) {
        this.subjCodeEqiv = subjCodeEqiv;
    }

    public String getCrseNumbEqiv() {
        return crseNumbEqiv;
    }

    public void setCrseNumbEqiv(String crseNumbEqiv) {
        this.crseNumbEqiv = crseNumbEqiv;
    }

    public String getStartTerm() {
        return startTerm;
    }

    public void setStartTerm(String startTerm) {
        this.startTerm = startTerm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScreqivKey)) return false;
        ScreqivKey that = (ScreqivKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(effTerm, that.effTerm) && Objects.equals(subjCodeEqiv, that.subjCodeEqiv) && Objects.equals(crseNumbEqiv, that.crseNumbEqiv) && Objects.equals(startTerm, that.startTerm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, effTerm, subjCodeEqiv, crseNumbEqiv, startTerm);
    }

    @Override
    public String toString() {
        return "ScreqivKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", effTerm='" + effTerm + '\'' +
                ", subjCodeEqiv='" + subjCodeEqiv + '\'' +
                ", crseNumbEqiv='" + crseNumbEqiv + '\'' +
                ", startTerm='" + startTerm + '\'' +
                '}';
    }
}
