package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScrrtstKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String termCodeEff;
    private Integer seqno;
    private String tescCode;

    public ScrrtstKey(String subjCode, String crseNumb, String termCodeEff, Integer seqno, String tescCode) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.termCodeEff = termCodeEff;
        this.seqno = seqno;
        this.tescCode = tescCode;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getTermCodeEff() {
        return termCodeEff;
    }

    public void setTermCodeEff(String termCodeEff) {
        this.termCodeEff = termCodeEff;
    }

    public Integer getSeqno() {
        return seqno;
    }

    public void setSeqno(Integer seqno) {
        this.seqno = seqno;
    }

    public String getTescCode() {
        return tescCode;
    }

    public void setTescCode(String tescCode) {
        this.tescCode = tescCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScrrtstKey)) return false;
        ScrrtstKey that = (ScrrtstKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(termCodeEff, that.termCodeEff) && Objects.equals(seqno, that.seqno) && Objects.equals(tescCode, that.tescCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, termCodeEff, seqno, tescCode);
    }

    @Override
    public String toString() {
        return "ScrrtstKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", termCodeEff='" + termCodeEff + '\'' +
                ", seqno='" + seqno + '\'' +
                ", tescCode='" + tescCode + '\'' +
                '}';
    }
}
