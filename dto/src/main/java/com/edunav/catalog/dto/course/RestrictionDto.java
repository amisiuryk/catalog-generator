package com.edunav.catalog.dto.course;

import java.util.ArrayList;
import java.util.List;

public class RestrictionDto {

    private List<String> exclude;
    private List<String> include;

    public RestrictionDto() {
    }

    public static RestrictionDto buildExcluded(List<String> list) {
        RestrictionDto restriction = new RestrictionDto();
        restriction.setExclude(new ArrayList<>(list));
        return restriction;
    }

    public static RestrictionDto buildIncluded(List<String> list) {
        RestrictionDto restriction = new RestrictionDto();
        restriction.setInclude(new ArrayList<>(list));
        return restriction;
    }

    public List<String> getInclude() {
        return include;
    }

    public void setInclude(List<String> include) {
        this.include = include;
    }

    public List<String> getExclude() {
        return exclude;
    }

    public void setExclude(List<String> exclude) {
        this.exclude = exclude;
    }
}
