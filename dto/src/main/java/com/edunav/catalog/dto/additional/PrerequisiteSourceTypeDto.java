package com.edunav.catalog.dto.additional;

public enum PrerequisiteSourceTypeDto {
    DESCRIPTION, DATA_SET;

    PrerequisiteSourceTypeDto() {
    }
}
