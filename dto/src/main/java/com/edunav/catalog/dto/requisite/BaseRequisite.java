package com.edunav.catalog.dto.requisite;

import com.edunav.catalog.dto.requisite.Requisite;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonSubTypes({
        @JsonSubTypes.Type(value = Requisite.class, name = "complex"),
//        @JsonSubTypes.Type(value = CumulativeGpaRequisite.class, name = "cumulative_gpa"),  //deprecated
//        @JsonSubTypes.Type(value = ClassificationRequisite.class, name = "classification"),  //deprecated
//        @JsonSubTypes.Type(value = CoursesRequisite.class, name = "course"),
//        @JsonSubTypes.Type(value = SectionRequisite.class, name = "section"),
//        @JsonSubTypes.Type(value = QualityPiontRequisite.class, name = "quality_point"),
//        @JsonSubTypes.Type(value = StudentGroupRequisite.class, name = "student_group"),
//        @JsonSubTypes.Type(value = MajorRequisite.class, name = "major"),  //deprecated
//        @JsonSubTypes.Type(value = ProgramRequisite.class, name = "program"),  //deprecated
//        @JsonSubTypes.Type(value = ConcentrationRequisite.class, name = "concentration"),  //deprecated
//        @JsonSubTypes.Type(value = PlacementRequisite.class, name = "placement"),  //deprecated
//        @JsonSubTypes.Type(value = ScoreRequisite.class, name = "score"),
//        @JsonSubTypes.Type(value = AdmissionRequisite.class, name = "admission"),  //deprecated
//        @JsonSubTypes.Type(value = EmptyRequisite.class, name = "empty"),
//        @JsonSubTypes.Type(value = ConditionRequisite.class, name = "condition")

})

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
public class BaseRequisite {
    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
