package com.edunav.catalog.dto.requisite;

import java.util.List;

public class Requisite extends BaseRequisite {
    private String connector;
    private String description;
    private String resolveMethod;
    private String stopRule;
    private Integer minLines;
    private Integer maxLines;
    private String courseSelectMethod;
    private Boolean partitionShare;
    private List<BaseRequisite> requisites = null;
    private Double requiredNumberOfConditions;
    private String areaCode;
    private String ruleCode;
    private String groupCode;
    private String connectorCond;
    private String connectorMaxCond;
    private String connectorReq;
    private String connectorMax;
    private String campCode;
    private String collCode;
    private Integer complCourses;
    private Double complCredits;
    private String deptCode;
    private Integer maxCourses;
    private Integer maxCoursesTransfer;
    private Double maxCredits;
    private Double maxCreditsTransfer;
    private Double maxCredCrse;
    private Double minCredCrse;
    private Integer reqCourses;
    private Double reqCredits;
    private Double maxCredCond;
    private Integer reqCoursesCond;
    private Double reqCredCond;
    private String connectorTransfer;
    private Integer maxCoursesCond;
    private Integer numberOfYears;
    private String fromTerm;
    private String toTerm;

    public String getCourseSelectMethod() {
        return courseSelectMethod;
    }

    public void setCourseSelectMethod(String courseSelectMethod) {
        this.courseSelectMethod = courseSelectMethod;
    }

    public String getResolveMethod() {
        return resolveMethod;
    }

    public void setResolveMethod(String resolveMethod) {
        this.resolveMethod = resolveMethod;
    }

    public String getStopRule() {
        return stopRule;
    }

    public void setStopRule(String stopRule) {
        this.stopRule = stopRule;
    }

    public Integer getMinLines() {
        return minLines;
    }

    public void setMinLines(Integer minLines) {
        this.minLines = minLines;
    }

    public Integer getMaxLines() {
        return maxLines;
    }

    public void setMaxLines(Integer maxLines) {
        this.maxLines = maxLines;
    }

    public Boolean getPartitionShare() {
        return partitionShare;
    }

    public void setPartitionShare(Boolean partitionShare) {
        this.partitionShare = partitionShare;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<BaseRequisite> getRequisites() {
        return requisites;
    }

    public void setRequisites(List<BaseRequisite> requisites) {
        this.requisites = requisites;
    }

    public String getConnector() {
        return connector;
    }

    public void setConnector(String connector) {
        this.connector = connector;
    }

    public Double getRequiredNumberOfConditions() {
        return requiredNumberOfConditions;
    }

    public void setRequiredNumberOfConditions(Double requiredNumberOfConditions) {
        this.requiredNumberOfConditions = requiredNumberOfConditions;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getConnectorCond() {
        return connectorCond;
    }

    public void setConnectorCond(String connectorCond) {
        this.connectorCond = connectorCond;
    }

    public String getConnectorMaxCond() {
        return connectorMaxCond;
    }

    public void setConnectorMaxCond(String connectorMaxCond) {
        this.connectorMaxCond = connectorMaxCond;
    }

    public String getConnectorReq() {
        return connectorReq;
    }

    public void setConnectorReq(String connectorReq) {
        this.connectorReq = connectorReq;
    }

    public String getConnectorMax() {
        return connectorMax;
    }

    public void setConnectorMax(String connectorMax) {
        this.connectorMax = connectorMax;
    }

    public String getCampCode() {
        return campCode;
    }

    public void setCampCode(String campCode) {
        this.campCode = campCode;
    }

    public String getCollCode() {
        return collCode;
    }

    public void setCollCode(String collCode) {
        this.collCode = collCode;
    }

    public Integer getComplCourses() {
        return complCourses;
    }

    public void setComplCourses(Integer complCourses) {
        this.complCourses = complCourses;
    }

    public Double getComplCredits() {
        return complCredits;
    }

    public void setComplCredits(Double complCredits) {
        this.complCredits = complCredits;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public Integer getMaxCourses() {
        return maxCourses;
    }

    public void setMaxCourses(Integer maxCourses) {
        this.maxCourses = maxCourses;
    }

    public Integer getMaxCoursesTransfer() {
        return maxCoursesTransfer;
    }

    public void setMaxCoursesTransfer(Integer maxCoursesTransfer) {
        this.maxCoursesTransfer = maxCoursesTransfer;
    }

    public Double getMaxCredits() {
        return maxCredits;
    }

    public void setMaxCredits(Double maxCredits) {
        this.maxCredits = maxCredits;
    }

    public Double getMaxCreditsTransfer() {
        return maxCreditsTransfer;
    }

    public void setMaxCreditsTransfer(Double maxCreditsTransfer) {
        this.maxCreditsTransfer = maxCreditsTransfer;
    }

    public Double getMaxCredCrse() {
        return maxCredCrse;
    }

    public void setMaxCredCrse(Double maxCredCrse) {
        this.maxCredCrse = maxCredCrse;
    }

    public Double getMinCredCrse() {
        return minCredCrse;
    }

    public void setMinCredCrse(Double minCredCrse) {
        this.minCredCrse = minCredCrse;
    }

    public Integer getReqCourses() {
        return reqCourses;
    }

    public void setReqCourses(Integer reqCourses) {
        this.reqCourses = reqCourses;
    }

    public Double getReqCredits() {
        return reqCredits;
    }

    public void setReqCredits(Double reqCredits) {
        this.reqCredits = reqCredits;
    }

    public Double getMaxCredCond() {
        return maxCredCond;
    }

    public void setMaxCredCond(Double maxCredCond) {
        this.maxCredCond = maxCredCond;
    }

    public Integer getReqCoursesCond() {
        return reqCoursesCond;
    }

    public void setReqCoursesCond(Integer reqCoursesCond) {
        this.reqCoursesCond = reqCoursesCond;
    }

    public Double getReqCredCond() {
        return reqCredCond;
    }

    public void setReqCredCond(Double reqCredCond) {
        this.reqCredCond = reqCredCond;
    }

    public String getConnectorTransfer() {
        return connectorTransfer;
    }

    public void setConnectorTransfer(String connectorTransfer) {
        this.connectorTransfer = connectorTransfer;
    }

    public Integer getMaxCoursesCond() {
        return maxCoursesCond;
    }

    public void setMaxCoursesCond(Integer maxCoursesCond) {
        this.maxCoursesCond = maxCoursesCond;
    }

    public Integer getNumberOfYears() {
        return numberOfYears;
    }

    public void setNumberOfYears(Integer numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    public String getFromTerm() {
        return fromTerm;
    }

    public void setFromTerm(String fromTerm) {
        this.fromTerm = fromTerm;
    }

    public String getToTerm() {
        return toTerm;
    }

    public void setToTerm(String toTerm) {
        this.toTerm = toTerm;
    }
}
