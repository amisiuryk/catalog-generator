package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScrcorqKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String effTerm;
    private String subjCodeCorq;
    private String crseNumbCorq;

    public ScrcorqKey(String subjCode, String crseNumb, String effTerm, String subjCodeCorq, String crseNumbCorq) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.effTerm = effTerm;
        this.subjCodeCorq = subjCodeCorq;
        this.crseNumbCorq = crseNumbCorq;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(String effTerm) {
        this.effTerm = effTerm;
    }

    public String getSubjCodeCorq() {
        return subjCodeCorq;
    }

    public void setSubjCodeCorq(String subjCodeCorq) {
        this.subjCodeCorq = subjCodeCorq;
    }

    public String getCrseNumbCorq() {
        return crseNumbCorq;
    }

    public void setCrseNumbCorq(String crseNumbCorq) {
        this.crseNumbCorq = crseNumbCorq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScrcorqKey)) return false;
        ScrcorqKey that = (ScrcorqKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(effTerm, that.effTerm) && Objects.equals(subjCodeCorq, that.subjCodeCorq) && Objects.equals(crseNumbCorq, that.crseNumbCorq);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, effTerm, subjCodeCorq, crseNumbCorq);
    }

    @Override
    public String toString() {
        return "ScrcorqKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", effTerm='" + effTerm + '\'' +
                ", subjCodeCorq='" + subjCodeCorq + '\'' +
                ", crseNumbCorq='" + crseNumbCorq + '\'' +
                '}';
    }
}
