package com.edunav.catalog.dto.requisite;

import java.util.List;

public class ScoreRequisite extends OperatorRequisite {

    private List<ScoreRequisiteItem> scores = null;

    public List<ScoreRequisiteItem> getScores() {
        return scores;
    }

    public void setScores(List<ScoreRequisiteItem> scores) {
        this.scores = scores;
    }


}
