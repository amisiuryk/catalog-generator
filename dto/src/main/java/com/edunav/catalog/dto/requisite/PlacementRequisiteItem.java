
package com.edunav.catalog.dto.requisite;


public class PlacementRequisiteItem {

    private String id;

    private String component;

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
