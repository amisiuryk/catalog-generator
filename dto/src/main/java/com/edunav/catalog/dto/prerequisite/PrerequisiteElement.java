package com.edunav.catalog.dto.prerequisite;

import com.edunav.catalog.dto.common.CourseRange;

import java.util.List;

/**
 * Created by kip on 3/18/15.
 */
//@JsonFilter("diffFilter")
public class PrerequisiteElement {

    private String id;
    private CourseRange range;
    private String subject;
    private String number;
    private boolean hidden = false;
    private String type;
    private String grade;
    private String maxGrade;
    private String gradeQualityPoints;
    //    private Boolean corequisite;
    private Boolean concurrency;
    private Boolean scheduleCourse;
    private Boolean antiRequisite;
    private String courseAttrCode;
    private String studentAttrCode;
    private String operator;
    private List<PrerequisiteExclusionModel> exclusions;
    private Integer numberOfYears;
    private String fromTerm;
    private String toTerm;
    private String campCode;
    private String collCode;
    private Integer complCourses;
    private Double complCredits;
    private String deptCode;
    private Integer maxCourses;
    private Integer maxCoursesTransfer;
    private Double maxCredits;
    private Double maxCreditsTransfer;
    private Double maxCredCrse;
    private Double minCredCrse;
    private Integer reqCourses;
    private Double reqCredits;
    private Double maxCredCond;
    private Integer reqCoursesCond;
    private Double reqCredCond;
    private String connectorReq;
    private String connectorMax;
    private String connectorTransfer;

    public String getGradeQualityPoints() {
        return gradeQualityPoints;
    }

    public void setGradeQualityPoints(String gradeQualityPoints) {
        this.gradeQualityPoints = gradeQualityPoints;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public Boolean getCorequisite() {
//        return corequisite;
//    }

//    public void setCorequisite(Boolean corequisite) {
//        this.corequisite = corequisite;
//    }

    public Boolean getScheduleCourse() {
        return scheduleCourse;
    }

    public void setScheduleCourse(Boolean scheduleCourse) {
        this.scheduleCourse = scheduleCourse;
    }

    public Boolean getAntiRequisite() {
        return antiRequisite;
    }

    public void setAntiRequisite(Boolean antiRequisite) {
        this.antiRequisite = antiRequisite;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(String maxGrade) {
        this.maxGrade = maxGrade;
    }

    @Override
//    public String toString() {
//        return id + (getCorequisite() != null && getCorequisite() ? "(c)" : "");
//    }
    public String toString() {
        return id + (getConcurrency() != null && getConcurrency() ? "(c)" : "");
    }


    public Boolean getConcurrency() {
        return concurrency;
    }

    public void setConcurrency(Boolean concurrency) {
        this.concurrency = concurrency;
    }

    public CourseRange getRange() {
        return range;
    }

    public void setRange(CourseRange range) {
        this.range = range;
    }

    public String getCourseAttrCode() {
        return courseAttrCode;
    }

    public void setCourseAttrCode(String courseAttrCode) {
        this.courseAttrCode = courseAttrCode;
    }

    public String getStudentAttrCode() {
        return studentAttrCode;
    }

    public void setStudentAttrCode(String studentAttrCode) {
        this.studentAttrCode = studentAttrCode;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public List<PrerequisiteExclusionModel> getExclusions() {
        return exclusions;
    }

    public void setExclusions(List<PrerequisiteExclusionModel> exclusions) {
        this.exclusions = exclusions;
    }

    public Integer getNumberOfYears() {
        return numberOfYears;
    }

    public void setNumberOfYears(Integer numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    public String getFromTerm() {
        return fromTerm;
    }

    public void setFromTerm(String fromTerm) {
        this.fromTerm = fromTerm;
    }

    public String getToTerm() {
        return toTerm;
    }

    public void setToTerm(String toTerm) {
        this.toTerm = toTerm;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCampCode() {
        return campCode;
    }

    public void setCampCode(String campCode) {
        this.campCode = campCode;
    }

    public String getCollCode() {
        return collCode;
    }

    public void setCollCode(String collCode) {
        this.collCode = collCode;
    }

    public Integer getComplCourses() {
        return complCourses;
    }

    public void setComplCourses(Integer complCourses) {
        this.complCourses = complCourses;
    }

    public Double getComplCredits() {
        return complCredits;
    }

    public void setComplCredits(Double complCredits) {
        this.complCredits = complCredits;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public Integer getMaxCourses() {
        return maxCourses;
    }

    public void setMaxCourses(Integer maxCourses) {
        this.maxCourses = maxCourses;
    }

    public Integer getMaxCoursesTransfer() {
        return maxCoursesTransfer;
    }

    public void setMaxCoursesTransfer(Integer maxCoursesTransfer) {
        this.maxCoursesTransfer = maxCoursesTransfer;
    }

    public Double getMaxCredits() {
        return maxCredits;
    }

    public void setMaxCredits(Double maxCredits) {
        this.maxCredits = maxCredits;
    }

    public Double getMaxCreditsTransfer() {
        return maxCreditsTransfer;
    }

    public void setMaxCreditsTransfer(Double maxCreditsTransfer) {
        this.maxCreditsTransfer = maxCreditsTransfer;
    }

    public Double getMaxCredCrse() {
        return maxCredCrse;
    }

    public void setMaxCredCrse(Double maxCredCrse) {
        this.maxCredCrse = maxCredCrse;
    }

    public Double getMinCredCrse() {
        return minCredCrse;
    }

    public void setMinCredCrse(Double minCredCrse) {
        this.minCredCrse = minCredCrse;
    }

    public Integer getReqCourses() {
        return reqCourses;
    }

    public void setReqCourses(Integer reqCourses) {
        this.reqCourses = reqCourses;
    }

    public Double getReqCredits() {
        return reqCredits;
    }

    public void setReqCredits(Double reqCredits) {
        this.reqCredits = reqCredits;
    }

    public Double getMaxCredCond() {
        return maxCredCond;
    }

    public void setMaxCredCond(Double maxCredCond) {
        this.maxCredCond = maxCredCond;
    }

    public Integer getReqCoursesCond() {
        return reqCoursesCond;
    }

    public void setReqCoursesCond(Integer reqCoursesCond) {
        this.reqCoursesCond = reqCoursesCond;
    }

    public Double getReqCredCond() {
        return reqCredCond;
    }

    public void setReqCredCond(Double reqCredCond) {
        this.reqCredCond = reqCredCond;
    }

    public String getConnectorReq() {
        return connectorReq;
    }

    public void setConnectorReq(String connectorReq) {
        this.connectorReq = connectorReq;
    }

    public String getConnectorMax() {
        return connectorMax;
    }

    public void setConnectorMax(String connectorMax) {
        this.connectorMax = connectorMax;
    }

    public String getConnectorTransfer() {
        return connectorTransfer;
    }

    public void setConnectorTransfer(String connectorTransfer) {
        this.connectorTransfer = connectorTransfer;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}