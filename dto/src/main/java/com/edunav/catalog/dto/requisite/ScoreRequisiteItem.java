
package com.edunav.catalog.dto.requisite;


public class ScoreRequisiteItem {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
