package com.edunav.catalog.dto.requisite;

public class OperatorRequisite extends BaseRequisite {
    private String operator;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }
}
