package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class CrseSimpleKey implements Serializable {

    private String subjCode;
    private String crseNumb;

    public CrseSimpleKey(String subjCode, String crseNumb) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CrseSimpleKey)) return false;
        CrseSimpleKey that = (CrseSimpleKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb);
    }

    @Override
    public String toString() {
        return "CrseSimpleKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                '}';
    }
}
