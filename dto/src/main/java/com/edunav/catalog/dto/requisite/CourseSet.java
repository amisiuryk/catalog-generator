package com.edunav.catalog.dto.requisite;

import com.edunav.catalog.dto.common.CourseRange;

import java.util.List;

public class CourseSet {
    private String courseSet;
    private Boolean includeEquivalent;
    private Boolean hidden;
    private CourseRange range;
    private List<Object> qualifiers;

    public Boolean getIncludeEquivalent() {
        return includeEquivalent;
    }

    public void setIncludeEquivalent(Boolean includeEquivalent) {
        this.includeEquivalent = includeEquivalent;
    }

    public CourseSet(String courseSet) {
        this.courseSet = courseSet;
    }

    public CourseSet() {

    }

    public String getCourseSet() {
        return courseSet;
    }

    public void setCourseSet(String courseSet) {
        this.courseSet = courseSet;
    }

    public List<Object> getQualifiers() {
        return qualifiers;
    }

    public void setQualifiers(List<Object> qualifiers) {
        this.qualifiers = qualifiers;
    }


    @Override
    public String toString() {
        StringBuilder string = new StringBuilder().append(courseSet);
        if (qualifiers != null && !qualifiers.isEmpty()) {
            string.append("[").append(qualifiers.toString()).append("]");
        }
        return string.toString();
    }

    public CourseRange getRange() {
        return range;
    }

    public void setRange(CourseRange range) {
        this.range = range;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
}
