package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScrmexcKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String effTerm;
    private String subjCodeMexc;
    private String crseNumbMexc;
    private String levlCode;
    private String startTerm;

    public ScrmexcKey(String subjCode, String crseNumb, String effTerm, String subjCodeMexc, String crseNumbMexc, String levlCode, String startTerm) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.effTerm = effTerm;
        this.subjCodeMexc = subjCodeMexc;
        this.crseNumbMexc = crseNumbMexc;
        this.levlCode = levlCode;
        this.startTerm = startTerm;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(String effTerm) {
        this.effTerm = effTerm;
    }

    public String getSubjCodeMexc() {
        return subjCodeMexc;
    }

    public void setSubjCodeMexc(String subjCodeMexc) {
        this.subjCodeMexc = subjCodeMexc;
    }

    public String getCrseNumbMexc() {
        return crseNumbMexc;
    }

    public void setCrseNumbMexc(String crseNumbMexc) {
        this.crseNumbMexc = crseNumbMexc;
    }

    public String getLevlCode() {
        return levlCode;
    }

    public void setLevlCode(String levlCode) {
        this.levlCode = levlCode;
    }

    public String getStartTerm() {
        return startTerm;
    }

    public void setStartTerm(String startTerm) {
        this.startTerm = startTerm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScrmexcKey)) return false;
        ScrmexcKey that = (ScrmexcKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(effTerm, that.effTerm) && Objects.equals(subjCodeMexc, that.subjCodeMexc) && Objects.equals(crseNumbMexc, that.crseNumbMexc) && Objects.equals(levlCode, that.levlCode) && Objects.equals(startTerm, that.startTerm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, effTerm, subjCodeMexc, crseNumbMexc, levlCode, startTerm);
    }

    @Override
    public String toString() {
        return "ScreqivKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", effTerm='" + effTerm + '\'' +
                ", subjCodeMexc='" + subjCodeMexc + '\'' +
                ", crseNumbMexc='" + crseNumbMexc + '\'' +
                ", levlCode='" + levlCode + '\'' +
                ", startTerm='" + startTerm + '\'' +
                '}';
    }
}
