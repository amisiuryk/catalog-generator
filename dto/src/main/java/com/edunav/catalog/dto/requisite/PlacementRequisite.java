package com.edunav.catalog.dto.requisite;

import java.util.List;

@Deprecated
public class PlacementRequisite extends OperatorRequisite {

    private List<PlacementRequisiteItem> placements = null;

    private List<ScoreRequisite> requisites = null;

    public List<ScoreRequisite> getRequisites() {
        return requisites;
    }

    public void setRequisites(List<ScoreRequisite> requisites) {
        this.requisites = requisites;
    }

    public List<PlacementRequisiteItem> getPlacements() {
        return placements;
    }

    public void setPlacements(List<PlacementRequisiteItem> placements) {
        this.placements = placements;
    }
}
