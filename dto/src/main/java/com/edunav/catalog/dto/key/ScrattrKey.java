package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScrattrKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String effTerm;
    private String attrCode;

    public ScrattrKey(String subjCode, String crseNumb, String effTerm, String attrCode) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.effTerm = effTerm;
        this.attrCode = attrCode;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(String effTerm) {
        this.effTerm = effTerm;
    }

    public String getAttrCode() {
        return attrCode;
    }

    public void setAttrCode(String attrCode) {
        this.attrCode = attrCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScrattrKey)) return false;
        ScrattrKey that = (ScrattrKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(effTerm, that.effTerm) && Objects.equals(attrCode, that.attrCode) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, effTerm, attrCode);
    }

    @Override
    public String toString() {
        return "ScrattrKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", effTerm='" + effTerm + '\'' +
                ", attrCode='" + attrCode + '\'' +
                '}';
    }
}
