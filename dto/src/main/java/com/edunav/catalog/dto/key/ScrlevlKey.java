package com.edunav.catalog.dto.key;

import java.io.Serializable;
import java.util.Objects;

public class ScrlevlKey implements Serializable {

    private String subjCode;
    private String crseNumb;
    private String effTerm;
    private String levlCode;

    public ScrlevlKey(String subjCode, String crseNumb, String effTerm, String levlCode) {
        this.subjCode = subjCode;
        this.crseNumb = crseNumb;
        this.effTerm = effTerm;
        this.levlCode = levlCode;
    }

    public String getSubjCode() {
        return subjCode;
    }

    public void setSubjCode(String subjCode) {
        this.subjCode = subjCode;
    }

    public String getCrseNumb() {
        return crseNumb;
    }

    public void setCrseNumb(String crseNumb) {
        this.crseNumb = crseNumb;
    }

    public String getEffTerm() {
        return effTerm;
    }

    public void setEffTerm(String effTerm) {
        this.effTerm = effTerm;
    }

    public String getLevlCode() {
        return levlCode;
    }

    public void setLevlCode(String levlCode) {
        this.levlCode = levlCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScrlevlKey)) return false;
        ScrlevlKey that = (ScrlevlKey) o;
        return Objects.equals(subjCode, that.subjCode) && Objects.equals(crseNumb, that.crseNumb) && Objects.equals(effTerm, that.effTerm) && Objects.equals(levlCode, that.levlCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subjCode, crseNumb, effTerm, levlCode);
    }

    @Override
    public String toString() {
        return "ScrcorqKey{" +
                "subjCode='" + subjCode + '\'' +
                ", crseNumb='" + crseNumb + '\'' +
                ", effTerm='" + effTerm + '\'' +
                ", levlCode='" + levlCode + '\'' +
                '}';
    }
}
