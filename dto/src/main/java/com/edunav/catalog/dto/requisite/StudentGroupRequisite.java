package com.edunav.catalog.dto.requisite;

import java.util.List;

//@Deprecated
public class StudentGroupRequisite extends OperatorRequisite {

    private List<StudentGroupRequisiteItem> groups = null;

    public List<StudentGroupRequisiteItem> getGroups() {
        return groups;
    }

    public void setGroups(List<StudentGroupRequisiteItem> groups) {
        this.groups = groups;
    }

}
