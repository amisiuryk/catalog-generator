package com.edunav.catalog.dto.requisite;

import java.util.List;

public class CoursesRequisite extends BaseRequisite {
    private Boolean hidden;
    private Boolean corequisite;
    private Boolean concurrency;
    private Double minCredits;
    private Double maxCredits;
    private MinGradeSet minGradeSet;
    private List<CourseSet> courses = null;
    private List<BaseRequisite> requisites = null;
    private List<CoursesRequisiteExclusion> exclusions = null;
    private Integer numberOfYears;
    private String fromTerm;
    private String toTerm;

    private String campCode;
    private String collCode;
    private Integer complCourses;
    private Double complCredits;
    private String deptCode;
    private Integer maxCourses;
    private Integer maxCoursesTransfer;
    private Double maxCreditsTransfer;
    private Double maxCredCrse;
    private Double minCredCrse;
    private Integer reqCourses;
    private Double reqCredits;
    private Double maxCredCond;
    private Integer reqCoursesCond;
    private Double reqCredCond;
    private String connectorReq;
    private String connectorMax;
    private String connectorTransfer;

    public Boolean getCorequisite() {
        return corequisite;
    }

    public void setCorequisite(Boolean corequisite) {
        this.corequisite = corequisite;
    }

    public Boolean getConcurrency() {
        return concurrency;
    }

    public void setConcurrency(Boolean concurrency) {
        this.concurrency = concurrency;
    }

    public Double getMinCredits() {
        return minCredits;
    }

    public void setMinCredits(Double minCredits) {
        this.minCredits = minCredits;
    }

    public Double getMaxCredits() {
        return maxCredits;
    }

    public void setMaxCredits(Double maxCredits) {
        this.maxCredits = maxCredits;
    }

//    public Double getMinClasses() {
//        return minClasses;
//    }

//    public void setMinClasses(Double minClasses) {
//        this.minClasses = minClasses;
//    }

//    public Double getMaxClasses() {
//        return maxClasses;
//    }

//    public void setMaxClasses(Double maxClasses) {
//        this.maxClasses = maxClasses;
//    }

    public MinGradeSet getMinGradeSet() {
        return minGradeSet;
    }

    public void setMinGradeSet(MinGradeSet minGradeSet) {
        this.minGradeSet = minGradeSet;
    }

    public List<CourseSet> getCourses() {
        return courses;
    }

    public void setCourses(List<CourseSet> courses) {
        this.courses = courses;
    }

    public List<BaseRequisite> getRequisites() {
        return requisites;
    }

    public void setRequisites(List<BaseRequisite> requisites) {
        this.requisites = requisites;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public List<CoursesRequisiteExclusion> getExclusions() {
        return exclusions;
    }

    public void setExclusions(List<CoursesRequisiteExclusion> exclusions) {
        this.exclusions = exclusions;
    }

    public Integer getNumberOfYears() {
        return numberOfYears;
    }

    public void setNumberOfYears(Integer numberOfYears) {
        this.numberOfYears = numberOfYears;
    }

    public String getFromTerm() {
        return fromTerm;
    }

    public void setFromTerm(String fromTerm) {
        this.fromTerm = fromTerm;
    }

    public String getToTerm() {
        return toTerm;
    }

    public void setToTerm(String toTerm) {
        this.toTerm = toTerm;
    }

    public String getCampCode() {
        return campCode;
    }

    public void setCampCode(String campCode) {
        this.campCode = campCode;
    }

    public String getCollCode() {
        return collCode;
    }

    public void setCollCode(String collCode) {
        this.collCode = collCode;
    }

    public Integer getComplCourses() {
        return complCourses;
    }

    public void setComplCourses(Integer complCourses) {
        this.complCourses = complCourses;
    }

    public Double getComplCredits() {
        return complCredits;
    }

    public void setComplCredits(Double complCredits) {
        this.complCredits = complCredits;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public Integer getMaxCourses() {
        return maxCourses;
    }

    public void setMaxCourses(Integer maxCourses) {
        this.maxCourses = maxCourses;
    }

    public Integer getMaxCoursesTransfer() {
        return maxCoursesTransfer;
    }

    public void setMaxCoursesTransfer(Integer maxCoursesTransfer) {
        this.maxCoursesTransfer = maxCoursesTransfer;
    }

    public Double getMaxCreditsTransfer() {
        return maxCreditsTransfer;
    }

    public void setMaxCreditsTransfer(Double maxCreditsTransfer) {
        this.maxCreditsTransfer = maxCreditsTransfer;
    }

    public Double getMaxCredCrse() {
        return maxCredCrse;
    }

    public void setMaxCredCrse(Double maxCredCrse) {
        this.maxCredCrse = maxCredCrse;
    }

    public Double getMinCredCrse() {
        return minCredCrse;
    }

    public void setMinCredCrse(Double minCredCrse) {
        this.minCredCrse = minCredCrse;
    }

    public Integer getReqCourses() {
        return reqCourses;
    }

    public void setReqCourses(Integer reqCourses) {
        this.reqCourses = reqCourses;
    }

    public Double getReqCredits() {
        return reqCredits;
    }

    public void setReqCredits(Double reqCredits) {
        this.reqCredits = reqCredits;
    }

    public Double getMaxCredCond() {
        return maxCredCond;
    }

    public void setMaxCredCond(Double maxCredCond) {
        this.maxCredCond = maxCredCond;
    }

    public Integer getReqCoursesCond() {
        return reqCoursesCond;
    }

    public void setReqCoursesCond(Integer reqCoursesCond) {
        this.reqCoursesCond = reqCoursesCond;
    }

    public Double getReqCredCond() {
        return reqCredCond;
    }

    public void setReqCredCond(Double reqCredCond) {
        this.reqCredCond = reqCredCond;
    }

    public String getConnectorReq() {
        return connectorReq;
    }

    public void setConnectorReq(String connectorReq) {
        this.connectorReq = connectorReq;
    }

    public String getConnectorMax() {
        return connectorMax;
    }

    public void setConnectorMax(String connectorMax) {
        this.connectorMax = connectorMax;
    }

    public String getConnectorTransfer() {
        return connectorTransfer;
    }

    public void setConnectorTransfer(String connectorTransfer) {
        this.connectorTransfer = connectorTransfer;
    }

}