package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScrtextRepository;
import com.edunav.catalog.dto.key.ScrtextKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScrtextLoader extends BaseLoader<ScrtextKey> {

    public ScrtextLoader(ScrtextRepository<ScrtextKey> scrtextRepository) {
        super(scrtextRepository);
    }
}
