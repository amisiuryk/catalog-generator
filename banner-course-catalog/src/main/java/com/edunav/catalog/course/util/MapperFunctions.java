package com.edunav.catalog.course.util;

import com.edunav.catalog.dto.key.CrseSimpleKey;
import com.edunav.catalog.dto.key.ScbcrkyKey;
import com.edunav.catalog.dto.key.ScbcrseKey;
import com.edunav.catalog.dto.key.ScbdescKey;
import com.edunav.catalog.dto.key.ScrattrKey;
import com.edunav.catalog.dto.key.ScrcorqKey;
import com.edunav.catalog.dto.key.ScreqivKey;
import com.edunav.catalog.dto.key.ScrlevlKey;
import com.edunav.catalog.dto.key.ScrmexcKey;
import com.edunav.catalog.dto.key.ScrrtstKey;
import com.edunav.catalog.dto.key.ScrtextKey;
import com.edunav.catalog.dto.key.ShrgrdeKey;
import com.edunav.catalog.dto.key.StvtermKey;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

public final class MapperFunctions {

    private MapperFunctions() {
    }

    public static final Function<Map<String, Object>, ScbcrseKey> SCBCRSE_KEY_MAPPER =
            it -> new ScbcrseKey((String) it.get("SCBCRSE_SUBJ_CODE"), (String) it.get("SCBCRSE_CRSE_NUMB"), (String) it.get("SCBCRSE_EFF_TERM"));
    public static final Function<Map<String, Object>, ScrtextKey> SCRTEXT_KEY_MAPPER = it -> new ScrtextKey(((BigDecimal) it.get("SCRTEXT_SURROGATE_ID")).intValue());
    public static final Function<Map<String, Object>, CrseSimpleKey> SCRTEXT_SIMPLE_CRSE_MAPPER =
            it -> new CrseSimpleKey((String) it.get("SCRTEXT_SUBJ_CODE"), (String) it.get("SCRTEXT_CRSE_NUMB"));
    public static final Function<Map<String, Object>, ScbdescKey> SCBDESC_KEY_MAPPER =
            it -> new ScbdescKey((String) it.get("SCBDESC_SUBJ_CODE"), (String) it.get("SCBDESC_CRSE_NUMB"), (String) it.get("SCBDESC_TERM_CODE_EFF"),
                    (String) it.get("SCBDESC_TERM_CODE_END"));
    public static final Function<Map<String, Object>, CrseSimpleKey> SCBDESC_SIMPLE_CRSE_MAPPER =
            it -> new CrseSimpleKey((String) it.get("SCBDESC_SUBJ_CODE"), (String) it.get("SCBDESC_CRSE_NUMB"));
    public static final Function<Map<String, Object>, ScbcrkyKey> SCBCRKY_KEY_MAPPER =
            it -> new ScbcrkyKey((String) it.get("SCBCRKY_SUBJ_CODE"), (String) it.get("SCBCRKY_CRSE_NUMB"));
    public static final Function<Map<String, Object>, CrseSimpleKey> SCBCRKY_SIMPLE_CRSE_MAPPER =
            it -> new CrseSimpleKey((String) it.get("SCBCRKY_SUBJ_CODE"), (String) it.get("SCBCRKY_CRSE_NUMB"));
    public static final Function<Map<String, Object>, StvtermKey> STVTERM_KEY_MAPPER =
            it -> new StvtermKey((String) it.get("STVTERM_CODE"));
    public static final Function<Map<String, Object>, ScrattrKey> SCRATTR_KEY_MAPPER =
            it -> new ScrattrKey((String) it.get("SCRATTR_SUBJ_CODE"), (String) it.get("SCRATTR_CRSE_NUMB"), (String) it.get("SCRATTR_EFF_TERM"),
                    (String) it.get("SCRATTR_ATTR_CODE"));
    public static final Function<Map<String, Object>, ScrcorqKey> SCRCORQ_KEY_MAPPER =
            it -> new ScrcorqKey((String) it.get("SCRCORQ_SUBJ_CODE"), (String) it.get("SCRCORQ_CRSE_NUMB"), (String) it.get("SCRCORQ_EFF_TERM"),
                    (String) it.get("SCRCORQ_SUBJ_CODE_CORQ"), (String) it.get("SCRCORQ_CRSE_NUMB_CORQ"));
    public static final Function<Map<String, Object>, ScreqivKey> SCREQIV_KEY_MAPPER =
            it -> new ScreqivKey((String) it.get("SCREQIV_SUBJ_CODE"), (String) it.get("SCREQIV_CRSE_NUMB"), (String) it.get("SCREQIV_EFF_TERM"),
                    (String) it.get("SCREQIV_SUBJ_CODE_EQIV"), (String) it.get("SCREQIV_CRSE_NUMB_EQIV"), (String) it.get("SCREQIV_START_TERM"));
    public static final Function<Map<String, Object>, ScrmexcKey> SCRMEXC_KEY_MAPPER =
            it -> new ScrmexcKey((String) it.get("SCRMEXC_SUBJ_CODE"), (String) it.get("SCRMEXC_CRSE_NUMB"), (String) it.get("SCRMEXC_EFF_TERM"),
                    (String) it.get("SCRMEXC_SUBJ_CODE_MEXC"), (String) it.get("SCRMEXC_CRSE_NUMB_MEXC"), (String) it.get("SCRMEXC_LEVL_CODE"), (String) it.get("SCRMEXC_START_TERM"));
    public static final Function<Map<String, Object>, ScrlevlKey> SCRLEVL_KEY_MAPPER =
            it -> new ScrlevlKey((String) it.get("SCRLEVL_SUBJ_CODE"), (String) it.get("SCRLEVL_CRSE_NUMB"), (String) it.get("SCRLEVL_EFF_TERM"),
                    (String) it.get("SCRLEVL_LEVL_CODE"));
    public static final Function<Map<String, Object>, ScrrtstKey> SCRRTST_KEY_MAPPER =
            it -> new ScrrtstKey((String) it.get("SCRRTST_SUBJ_CODE"), (String) it.get("SCRRTST_CRSE_NUMB"), (String) it.get("SCRRTST_TERM_CODE_EFF"),
                    it.get("SCRRTST_SEQNO") == null ? null : ((BigDecimal) it.get("SCRRTST_SEQNO")).intValue(), (String) it.get("SCRRTST_TESC_CODE"));
    public static final Function<Map<String, Object>, ShrgrdeKey> SHRGRDE_KEY_MAPPER =
            it -> new ShrgrdeKey((String) it.get("SHRGRDE_CODE"), (String) it.get("SHRGRDE_LEVL_CODE"), (String) it.get("SHRGRDE_TERM_CODE_EFFECTIVE"));

    public static final String ID_FORMATTER = "%s %s";
}
