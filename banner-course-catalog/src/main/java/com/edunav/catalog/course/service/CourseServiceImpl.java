package com.edunav.catalog.course.service;

import com.edunav.catalog.course.cache.CacheClient;
import com.edunav.catalog.course.converter.CourseConverter;
import com.edunav.catalog.course.converter.GradeConverter;
import com.edunav.catalog.dto.additional.Grade;
import com.edunav.catalog.dto.course.CourseDto;
import com.edunav.catalog.dto.key.ScbcrseKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Service
public class CourseServiceImpl implements CourseService {

    private final Logger logger = LoggerFactory.getLogger(CourseServiceImpl.class);

    private final CacheClient cacheClient;
    private final CourseConverter courseConverter;
    private final GradeConverter gradeConverter;

    @Autowired
    public CourseServiceImpl(CacheClient cacheClient, CourseConverter courseConverter, GradeConverter gradeConverter) {
        this.cacheClient = cacheClient;
        this.courseConverter = courseConverter;
        this.gradeConverter = gradeConverter;
    }

    @Override
    public List<Map<String, Object>> getAllTest() {
        return cacheClient.getAllTest();
    }

    @Override
    public List<CourseDto> getAllCourses() {
        logger.info("Get all courses");
        Map<ScbcrseKey, CourseDto> baseCourses = courseConverter.createBaseCourses(cacheClient.getAllScbcrse());
        courseConverter.addDescriptions(baseCourses, cacheClient.getAllScrtext());
        courseConverter.addNarrativeDescriptions(baseCourses, cacheClient.getAllScbdesc());
        courseConverter.addTermDates(baseCourses, cacheClient.getAllScbcrky());
        courseConverter.addYears(baseCourses, cacheClient.getAllStvterm());
        courseConverter.addAttributes(baseCourses, cacheClient.getAllScrattr());
        courseConverter.addCorequisites(baseCourses, cacheClient.getAllScrcorq());
        courseConverter.addEquivalents(baseCourses, cacheClient.getAllScreqiv());
        courseConverter.addMutuallyExclusive(baseCourses, cacheClient.getAllScrmexc());
        courseConverter.addLevelCodes(baseCourses, cacheClient.getAllScrlevl());
        Map<String, Double> gradesMap = gradeConverter.createGrades(cacheClient.getAllShrgrde()).stream().collect(toMap(Grade::getGrade, Grade::getQualityPoints, (o1, o2) -> o2));
        courseConverter.createPrerequisite(baseCourses, cacheClient.getAllScrrtst(), gradesMap);
        return new ArrayList<>(baseCourses.values());
    }

    @Override
    public Map<String, Object> getCourse(String p1, String p2, String p3) {
        ScbcrseKey scbcrseKey = new ScbcrseKey(p1, p2, p3);
        logger.info("Get course by key {}", scbcrseKey);
        return cacheClient.getScbcrseByKey(scbcrseKey);
    }
}
