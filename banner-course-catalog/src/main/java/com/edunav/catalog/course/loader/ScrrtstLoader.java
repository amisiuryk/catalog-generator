package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScrrtstRepository;
import com.edunav.catalog.dto.key.ScrrtstKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScrrtstLoader extends BaseLoader<ScrrtstKey> {

    public ScrrtstLoader(ScrrtstRepository<ScrrtstKey> scrrtstRepository) {
        super(scrrtstRepository);
    }
}
