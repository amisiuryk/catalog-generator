package com.edunav.catalog.course.parser;

import java.util.List;

public interface ExpressionComposer<T> {

    List<Object> composeExpression(T expression);
}
