package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScbcrkyKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScbcrkyRepositoryImpl extends BaseRepositoryImpl<ScbcrkyKey> implements ScbcrkyRepository<ScbcrkyKey> {

    @Autowired
    public ScbcrkyRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCBCRKY";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCBCRKY_SUBJ_CODE", "SCBCRKY_CRSE_NUMB", "SCBCRKY_TERM_CODE_START", "SCBCRKY_TERM_CODE_END");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCBCRKY_SUBJ_CODE", "SCBCRKY_CRSE_NUMB");
    }

    @Override
    protected Function<Map<String, Object>, ScbcrkyKey> getKeyMapper() {
        return MapperFunctions.SCBCRKY_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScbcrkyKey key) {
        return String.format("select %s from %s where SCBCRKY_SUBJ_CODE=%s and SCBCRKY_CRSE_NUMB=%s", String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScbcrkyKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCBCRKY_SUBJ_CODE, SCBCRKY_CRSE_NUMB) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScbcrkyKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScbcrkyKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s')", key.getSubjCode(), key.getCrseNumb()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
