package com.edunav.catalog.course.repository;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface BaseRepository<K> {
    List<K> findAllKeys();

    Map<String, Object> findByKey(K key);

    Map<K, Map<String, Object>> findByKeys(Collection<K> keys);
}
