package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScrlevlRepository;
import com.edunav.catalog.dto.key.ScrlevlKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScrlevlLoader extends BaseLoader<ScrlevlKey> {

    public ScrlevlLoader(ScrlevlRepository<ScrlevlKey> scrlevlRepository) {
        super(scrlevlRepository);
    }
}
