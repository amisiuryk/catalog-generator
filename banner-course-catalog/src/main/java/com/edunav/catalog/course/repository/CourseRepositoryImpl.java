package com.edunav.catalog.course.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class CourseRepositoryImpl implements CourseRepository {

    private final Logger logger = LoggerFactory.getLogger(CourseRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public CourseRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcTemplate.setFetchSize(2000);
    }

    @Override
    public List<Map<String, Object>> getAllCourses() {
        String query = "select * from SATURN.SCBCRSE";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(query);
        return maps;
    }
}
