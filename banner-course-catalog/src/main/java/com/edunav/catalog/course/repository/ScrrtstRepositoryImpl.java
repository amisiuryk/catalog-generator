package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScrrtstKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScrrtstRepositoryImpl extends BaseRepositoryImpl<ScrrtstKey> implements ScrrtstRepository<ScrrtstKey> {

    @Autowired
    public ScrrtstRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCRRTST";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCRRTST_SUBJ_CODE", "SCRRTST_CRSE_NUMB", "SCRRTST_TERM_CODE_EFF", "SCRRTST_SEQNO", "SCRRTST_TESC_CODE", "SCRRTST_TEST_SCORE",
                "SCRRTST_SUBJ_CODE_PREQ", "SCRRTST_CRSE_NUMB_PREQ", "SCRRTST_CONNECTOR", "SCRRTST_LPAREN", "SCRRTST_RPAREN", "SCRRTST_CONCURRENCY_IND", "SCRRTST_MIN_GRDE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCRRTST_SUBJ_CODE", "SCRRTST_CRSE_NUMB", "SCRRTST_TERM_CODE_EFF", "SCRRTST_SEQNO", "SCRRTST_TESC_CODE");
    }

    @Override
    protected Function<Map<String, Object>, ScrrtstKey> getKeyMapper() {
        return MapperFunctions.SCRRTST_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScrrtstKey key) {
        return String.format("select %s from %s where SCRRTST_SUBJ_CODE=%s and SCRRTST_CRSE_NUMB=%s and SCRRTST_TERM_CODE_EFF=%s and SCRRTST_SEQNO=%s and SCRRTST_TESC_CODE=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getTermCodeEff(), key.getSeqno(), key.getTescCode());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScrrtstKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCRRTST_SUBJ_CODE, SCRRTST_CRSE_NUMB, SCRRTST_TERM_CODE_EFF, coalesce(SCRRTST_SEQNO, null), coalesce(SCRRTST_TESC_CODE, 'null')) " +
                "IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScrrtstKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScrrtstKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s', %s, '%s')", key.getSubjCode(), key.getCrseNumb(), key.getTermCodeEff(), key.getSeqno(), key.getTescCode()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
