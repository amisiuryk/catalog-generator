package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.StvtermKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class StvtermRepositoryImpl extends BaseRepositoryImpl<StvtermKey> implements StvtermRepository<StvtermKey> {

    @Autowired
    public StvtermRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.STVTERM";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("STVTERM_CODE", "STVTERM_ACYR_CODE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("STVTERM_CODE");
    }

    @Override
    protected Function<Map<String, Object>, StvtermKey> getKeyMapper() {
        return MapperFunctions.STVTERM_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(StvtermKey key) {
        return String.format("select %s from %s where STVTERM_CODE=%s", String.join(", ", getAllColumns()), getTable(), key.getCode());
    }

    @Override
    protected String getListByKeysQuery(Collection<StvtermKey> keys) {
        String baseQuery = String.format("Select %s from %s where STVTERM_CODE IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<StvtermKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            StvtermKey key = iterator.next();
            queryBuilder.append(key.getCode());
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
