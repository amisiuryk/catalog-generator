package com.edunav.catalog.course.web;

import com.edunav.catalog.course.repository.CourseRepository;
import com.edunav.catalog.course.service.CourseService;
import com.edunav.catalog.dto.course.CourseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class CourseController {

    private CourseService courseService;
    private CourseRepository courseRepository;

    @Autowired
    public CourseController(CourseService courseService, CourseRepository courseRepository) {
        this.courseService = courseService;
        this.courseRepository = courseRepository;
    }

    @GetMapping(path = "courses/{id}")
    public Map<String, Object> getCourseById(@PathVariable String id) {
        String[] split = id.split("-");
        return courseService.getCourse(split[0], split[1], split[2]);
    }

    @GetMapping(path = "courses")
    public List<CourseDto> getAllCourses() {
        return courseService.getAllCourses();
    }

    @GetMapping(path = "courses/db")
    public List<Map<String, Object>> getAllDbCourses() {
        return courseRepository.getAllCourses();
    }

    @GetMapping(path = "test")
    public List<Map<String, Object>> getAllTest() {
        return courseService.getAllTest();
    }

}
