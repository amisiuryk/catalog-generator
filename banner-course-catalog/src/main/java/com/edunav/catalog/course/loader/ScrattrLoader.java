package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScrattrRepository;
import com.edunav.catalog.dto.key.ScrattrKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScrattrLoader extends BaseLoader<ScrattrKey> {

    public ScrattrLoader(ScrattrRepository<ScrattrKey> scrattrRepository) {
        super(scrattrRepository);
    }
}
