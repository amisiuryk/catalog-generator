package com.edunav.catalog.course.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.ColumnMapRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public abstract class BaseRepositoryImpl<K> implements BaseRepository<K> {

    private final Logger logger = LoggerFactory.getLogger(BaseRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final ColumnMapRowMapper columnMapRowMapper;

    public BaseRepositoryImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.jdbcTemplate.setFetchSize(2000);
        this.columnMapRowMapper = new ColumnMapRowMapper();
    }

    protected abstract String getTable();

    protected abstract List<String> getAllColumns();

    protected abstract List<String> getKeyColumns();

    protected abstract Function<Map<String, Object>, K> getKeyMapper();

    protected abstract String getOneByKeyQuery(K key);

    protected abstract String getListByKeysQuery(Collection<K> keys);

    public List<K> findAllKeys() {
        logger.info("Find all keys repo");
        String query = String.format("select %s from %s", String.join(", ", getKeyColumns()), getTable());
        List<Map<String, Object>> rawMaps = jdbcTemplate.queryForList(query);
        return rawMaps.stream()
                .map(getKeyMapper())
                .collect(Collectors.toList());
    }

    public Map<String, Object> findByKey(K key) {
        logger.info("Find repo by key {}", key);
        Map<String, Object> result = jdbcTemplate.queryForObject(getOneByKeyQuery(key), columnMapRowMapper);
        return result;
    }

    public Map<K, Map<String, Object>> findByKeys(Collection<K> keys) {
        logger.info("Find repo by keys {}", keys.size());
        List<Map<String, Object>> result = jdbcTemplate.queryForList(getListByKeysQuery(keys));
        return result.stream()
                .collect(toMap(getKeyMapper(), Function.identity()));
    }

}
