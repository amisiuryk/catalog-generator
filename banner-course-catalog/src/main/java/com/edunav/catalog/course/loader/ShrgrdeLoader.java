package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ShrgrdeRepository;
import com.edunav.catalog.dto.key.ShrgrdeKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ShrgrdeLoader extends BaseLoader<ShrgrdeKey> {

    public ShrgrdeLoader(ShrgrdeRepository<ShrgrdeKey> shrgrdeRepository) {
        super(shrgrdeRepository);
    }
}
