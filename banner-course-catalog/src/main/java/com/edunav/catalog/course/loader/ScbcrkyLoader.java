package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScbcrkyRepository;
import com.edunav.catalog.dto.key.ScbcrkyKey;
import com.hazelcast.spring.context.SpringAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringAware
public class ScbcrkyLoader extends BaseLoader<ScbcrkyKey> {

    private final Logger logger = LoggerFactory.getLogger(ScbcrkyLoader.class);

    public ScbcrkyLoader(ScbcrkyRepository<ScbcrkyKey> scbcrkyRepository) {
        super(scbcrkyRepository);
    }
}
