package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ShrgrdeKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ShrgrdeRepositoryImpl extends BaseRepositoryImpl<ShrgrdeKey> implements ShrgrdeRepository<ShrgrdeKey> {

    @Autowired
    public ShrgrdeRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SHRGRDE";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SHRGRDE_CODE", "SHRGRDE_ABBREV", "SHRGRDE_LEVL_CODE", "SHRGRDE_TERM_CODE_EFFECTIVE", "SHRGRDE_QUALITY_POINTS", "SHRGRDE_ATTEMPTED_IND", "SHRGRDE_COMPLETED_IND", "SHRGRDE_PASSED_IND", "SHRGRDE_GPA_IND", "SHRGRDE_NUMERIC_VALUE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SHRGRDE_CODE", "SHRGRDE_LEVL_CODE", "SHRGRDE_TERM_CODE_EFFECTIVE");
    }

    @Override
    protected Function<Map<String, Object>, ShrgrdeKey> getKeyMapper() {
        return MapperFunctions.SHRGRDE_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ShrgrdeKey key) {
        return String.format("select %s from %s where SHRGRDE_CODE=%s and SHRGRDE_LEVL_CODE=%s and SHRGRDE_TERM_CODE_EFFECTIVE=%s",
                String.join(", ", getAllColumns()), getTable(), key.getCode(), key.getLevlCode(), key.getTermCodeEffective());
    }

    @Override
    protected String getListByKeysQuery(Collection<ShrgrdeKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SHRGRDE_CODE, SHRGRDE_LEVL_CODE, SHRGRDE_TERM_CODE_EFFECTIVE) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ShrgrdeKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ShrgrdeKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s')", key.getCode(), key.getLevlCode(), key.getTermCodeEffective()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
