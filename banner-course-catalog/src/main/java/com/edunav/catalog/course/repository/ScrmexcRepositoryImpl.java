package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScrmexcKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScrmexcRepositoryImpl extends BaseRepositoryImpl<ScrmexcKey> implements ScrmexcRepository<ScrmexcKey> {

    @Autowired
    public ScrmexcRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCRMEXC";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCRMEXC_SUBJ_CODE", "SCRMEXC_CRSE_NUMB", "SCRMEXC_EFF_TERM", "SCRMEXC_SUBJ_CODE_MEXC", "SCRMEXC_CRSE_NUMB_MEXC", "SCRMEXC_LEVL_CODE", "SCRMEXC_START_TERM");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCRMEXC_SUBJ_CODE", "SCRMEXC_CRSE_NUMB", "SCRMEXC_EFF_TERM", "SCRMEXC_SUBJ_CODE_MEXC", "SCRMEXC_CRSE_NUMB_MEXC", "SCRMEXC_LEVL_CODE", "SCRMEXC_START_TERM");
    }

    @Override
    protected Function<Map<String, Object>, ScrmexcKey> getKeyMapper() {
        return MapperFunctions.SCRMEXC_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScrmexcKey key) {
        return String.format("select %s from %s where SCRMEXC_SUBJ_CODE=%s and SCRMEXC_CRSE_NUMB=%s and SCRMEXC_EFF_TERM=%s and SCRMEXC_SUBJ_CODE_MEXC=%s and SCRMEXC_CRSE_NUMB_MEXC=%s and SCRMEXC_LEVL_CODE=%s and SCRMEXC_START_TERM=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getSubjCodeMexc(), key.getCrseNumbMexc(), key.getLevlCode(), key.getStartTerm());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScrmexcKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCRMEXC_SUBJ_CODE, SCRMEXC_CRSE_NUMB, SCRMEXC_EFF_TERM, " +
                "coalesce(SCRMEXC_SUBJ_CODE_MEXC, 'null'), coalesce(SCRMEXC_CRSE_NUMB_MEXC, 'null'), coalesce(SCRMEXC_LEVL_CODE, 'null'), coalesce(SCRMEXC_START_TERM, 'null')) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScrmexcKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScrmexcKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s','%s','%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getSubjCodeMexc(), key.getCrseNumbMexc(), key.getLevlCode(), key.getStartTerm()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
