package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.StvtermRepository;
import com.edunav.catalog.dto.key.StvtermKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class StvtermLoader extends BaseLoader<StvtermKey> {

    public StvtermLoader(StvtermRepository<StvtermKey> stvtermRepository) {
        super(stvtermRepository);
    }
}
