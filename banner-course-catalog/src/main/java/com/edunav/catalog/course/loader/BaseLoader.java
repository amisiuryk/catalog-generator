package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.BaseRepository;
import com.hazelcast.map.MapLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Map;

public abstract class BaseLoader<K> implements MapLoader<K, Map<String, Object>> {

    private BaseRepository<K> baseRepository;

    private final Logger logger = LoggerFactory.getLogger(BaseLoader.class);

    public BaseLoader(BaseRepository<K> baseRepository) {
        this.baseRepository = baseRepository;
    }

    @Override
    public Map<String, Object> load(K key) {
        logger.info("Load by key {}", key);
        return baseRepository.findByKey(key);
    }

    @Override
    public Map<K, Map<String, Object>> loadAll(Collection<K> keys) {
        logger.info("Load all");
        return baseRepository.findByKeys(keys);
    }

    @Override
    public Iterable<K> loadAllKeys() {
        logger.info("Load all keys");
        return baseRepository.findAllKeys();
    }
}
