package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScbcrseKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;

@Repository
public class ScbcrseRepositoryImpl extends BaseRepositoryImpl<ScbcrseKey> implements ScbcrseRepository<ScbcrseKey> {

    private final Logger logger = LoggerFactory.getLogger(ScbcrseRepositoryImpl.class);

    @Autowired
    public ScbcrseRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCBCRSE";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCBCRSE_SUBJ_CODE", "SCBCRSE_CRSE_NUMB", "SCBCRSE_EFF_TERM", "SCBCRSE_COLL_CODE", "SCBCRSE_TITLE",
                "SCBCRSE_CSTA_CODE", "SCBCRSE_CREDIT_HR_LOW", "SCBCRSE_CREDIT_HR_HIGH", "SCBCRSE_CREDIT_HR_IND", "SCBCRSE_REPEAT_LIMIT", "SCBCRSE_MAX_RPT_UNITS",
                "SCBCRSE_CREDIT_STATUS_CODE", "SCBCRSE_DEPT_CODE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCBCRSE_SUBJ_CODE", "SCBCRSE_CRSE_NUMB", "SCBCRSE_EFF_TERM");
    }

    @Override
    protected Function getKeyMapper() {
        return MapperFunctions.SCBCRSE_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScbcrseKey key) {
        return String.format("select %s from %s where SCBCRSE_SUBJ_CODE=%s and SCBCRSE_CRSE_NUMB=%s and SCBCRSE_EFF_TERM=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getEffTerm());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScbcrseKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCBCRSE_SUBJ_CODE, SCBCRSE_CRSE_NUMB, SCBCRSE_EFF_TERM) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScbcrseKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScbcrseKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getEffTerm()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
