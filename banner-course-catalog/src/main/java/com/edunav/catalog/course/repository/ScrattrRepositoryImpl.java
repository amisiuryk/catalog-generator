package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScrattrKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScrattrRepositoryImpl extends BaseRepositoryImpl<ScrattrKey> implements ScrattrRepository<ScrattrKey> {

    private final Logger logger = LoggerFactory.getLogger(ScrattrRepositoryImpl.class);

    @Autowired
    public ScrattrRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCRATTR";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCRATTR_SUBJ_CODE", "SCRATTR_CRSE_NUMB", "SCRATTR_EFF_TERM", "SCRATTR_ATTR_CODE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCRATTR_SUBJ_CODE", "SCRATTR_CRSE_NUMB", "SCRATTR_EFF_TERM", "SCRATTR_ATTR_CODE");
    }

    @Override
    protected Function getKeyMapper() {
        return MapperFunctions.SCRATTR_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScrattrKey key) {
        return String.format("select %s from %s where SCRATTR_SUBJ_CODE=%s and SCRATTR_CRSE_NUMB=%s and SCRATTR_EFF_TERM=%s and SCRATTR_ATTR_CODE=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getAttrCode());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScrattrKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCRATTR_SUBJ_CODE, SCRATTR_CRSE_NUMB, SCRATTR_EFF_TERM, coalesce(SCRATTR_ATTR_CODE, 'null')) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScrattrKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScrattrKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getAttrCode()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
