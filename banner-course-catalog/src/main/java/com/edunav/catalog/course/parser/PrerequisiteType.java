package com.edunav.catalog.course.parser;

public enum PrerequisiteType {

    COURSE("course"), PLACEMENT("placement"), SECTION("section"), STUDENT_GROUP_ATTR("student_group");

    private String type;

    PrerequisiteType(String placement) {
        this.type = placement;
    }

    public String getType() {
        return type;
    }
}
