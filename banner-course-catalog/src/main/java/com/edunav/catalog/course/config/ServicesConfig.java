package com.edunav.catalog.course.config;

import com.edunav.ContextualThreadPoolExecutor;
import com.hazelcast.com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

@Configuration
public class ServicesConfig {

    @Bean(name = "sqlExecutor")
    public ExecutorService sqlExecutor() {
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("sql-%d").setDaemon(true).build();
        return ContextualThreadPoolExecutor.contextualThreadPoolExecutor(0, 10, 0,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>(), threadFactory);
    }

}
