package com.edunav.catalog.course.config;

import com.edunav.catalog.course.cache.CacheClient;
import com.edunav.catalog.course.loader.ScbcrkyLoader;
import com.edunav.catalog.course.loader.ScbcrseLoader;
import com.edunav.catalog.course.loader.ScbdescLoader;
import com.edunav.catalog.course.loader.ScrattrLoader;
import com.edunav.catalog.course.loader.ScrcorqLoader;
import com.edunav.catalog.course.loader.ScreqivLoader;
import com.edunav.catalog.course.loader.ScrlevlLoader;
import com.edunav.catalog.course.loader.ScrmexcLoader;
import com.edunav.catalog.course.loader.ScrrtstLoader;
import com.edunav.catalog.course.loader.ScrtextLoader;
import com.edunav.catalog.course.loader.ShrgrdeLoader;
import com.edunav.catalog.course.loader.StvtermLoader;
import com.edunav.catalog.course.repository.ScbcrkyRepository;
import com.edunav.catalog.course.repository.ScbcrseRepository;
import com.edunav.catalog.course.repository.ScbdescRepository;
import com.edunav.catalog.course.repository.ScrattrRepository;
import com.edunav.catalog.course.repository.ScrcorqRepository;
import com.edunav.catalog.course.repository.ScreqivRepository;
import com.edunav.catalog.course.repository.ScrlevlRepository;
import com.edunav.catalog.course.repository.ScrmexcRepository;
import com.edunav.catalog.course.repository.ScrrtstRepository;
import com.edunav.catalog.course.repository.ScrtextRepository;
import com.edunav.catalog.course.repository.ShrgrdeRepository;
import com.edunav.catalog.course.repository.StvtermRepository;
import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.context.SpringManagedContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class HazelCastConfiguration {

    @Bean
    public HazelcastInstance hazelcastInstance(ScbcrseRepository scbcrseRepository, ScrtextRepository scrtextRepository,
                                               ScbdescRepository scbdescRepository, ScbcrkyRepository scbcrkyRepository,
                                               StvtermRepository stvtermRepository, ScrattrRepository scrattrRepository,
                                               ScrcorqRepository scrcorqRepository, ScreqivRepository screqivRepository,
                                               ScrmexcRepository scrmexcRepository, ScrlevlRepository scrlevlRepository,
                                               ScrrtstRepository scrrtstRepository, ShrgrdeRepository shrgrdeRepository) {
        Config config = new Config();
        config.setClusterName("dev");
        config.setManagedContext(managedContext());
        config.setInstanceName("local");
        config.getProperties().put("hazelcast.discovery.enabled", true);

        NetworkConfig networkConfig = config.getNetworkConfig();
        JoinConfig join = networkConfig.getJoin();
        join.getTcpIpConfig().setEnabled(true).setMembers(List.of("127.0.0.1"));

        config.addMapConfig(getScbcrseMapConfig(scbcrseRepository));
        config.addMapConfig(getScrtextMapConfig(scrtextRepository));
        config.addMapConfig(getScbdescMapConfig(scbdescRepository));
        config.addMapConfig(getScbcrkyMapConfig(scbcrkyRepository));
        config.addMapConfig(getStvtermMapConfig(stvtermRepository));
        config.addMapConfig(getScrattrMapConfig(scrattrRepository));
        config.addMapConfig(getScrcorqMapConfig(scrcorqRepository));
        config.addMapConfig(getScreqivMapConfig(screqivRepository));
        config.addMapConfig(getScrmexcMapConfig(scrmexcRepository));
        config.addMapConfig(getScrlevlMapConfig(scrlevlRepository));
        config.addMapConfig(getScrrtstMapConfig(scrrtstRepository));
        config.addMapConfig(getShrgrdeMapConfig(shrgrdeRepository));
        return Hazelcast.newHazelcastInstance(config);
    }

    private MapConfig getScbcrseMapConfig(ScbcrseRepository scbcrseRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScbcrseLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScbcrseLoader scbcrseLoader = new ScbcrseLoader(scbcrseRepository);
        mapStoreConfig.setImplementation(scbcrseLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCBCRSE);
        mapConfig.setName(CacheClient.SCBCRSE);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScrtextMapConfig(ScrtextRepository scrtextRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScrtextLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScrtextLoader scrtextLoader = new ScrtextLoader(scrtextRepository);
        mapStoreConfig.setImplementation(scrtextLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCRTEXT);
        mapConfig.setName(CacheClient.SCRTEXT);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScbdescMapConfig(ScbdescRepository scbdescRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScbdescLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScbdescLoader scbdescLoader = new ScbdescLoader(scbdescRepository);
        mapStoreConfig.setImplementation(scbdescLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCBDESC);
        mapConfig.setName(CacheClient.SCBDESC);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScbcrkyMapConfig(ScbcrkyRepository scbcrkyRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScbcrkyLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScbcrkyLoader scbcrkyLoader = new ScbcrkyLoader(scbcrkyRepository);
        mapStoreConfig.setImplementation(scbcrkyLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCBCRKY);
        mapConfig.setName(CacheClient.SCBCRKY);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getStvtermMapConfig(StvtermRepository stvtermRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.StvtermLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        StvtermLoader stvtermLoader = new StvtermLoader(stvtermRepository);
        mapStoreConfig.setImplementation(stvtermLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.STVTERM);
        mapConfig.setName(CacheClient.STVTERM);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScrattrMapConfig(ScrattrRepository scrattrRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScrattrLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.LAZY);

        ScrattrLoader scrattrLoader = new ScrattrLoader(scrattrRepository);
        mapStoreConfig.setImplementation(scrattrLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCRATTR);
        mapConfig.setName(CacheClient.SCRATTR);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScrcorqMapConfig(ScrcorqRepository scrcorqRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScrcorqLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScrcorqLoader scrcorqLoader = new ScrcorqLoader(scrcorqRepository);
        mapStoreConfig.setImplementation(scrcorqLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCRCORQ);
        mapConfig.setName(CacheClient.SCRCORQ);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScreqivMapConfig(ScreqivRepository screqivRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScreqivLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScreqivLoader screqivLoader = new ScreqivLoader(screqivRepository);
        mapStoreConfig.setImplementation(screqivLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCREQIV);
        mapConfig.setName(CacheClient.SCREQIV);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScrmexcMapConfig(ScrmexcRepository scrmexcRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScrmexcLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScrmexcLoader scrmexcLoader = new ScrmexcLoader(scrmexcRepository);
        mapStoreConfig.setImplementation(scrmexcLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCRMEXC);
        mapConfig.setName(CacheClient.SCRMEXC);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScrlevlMapConfig(ScrlevlRepository scrlevlRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScrlevlLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScrlevlLoader scrlevlLoader = new ScrlevlLoader(scrlevlRepository);
        mapStoreConfig.setImplementation(scrlevlLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCRLEVL);
        mapConfig.setName(CacheClient.SCRLEVL);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getScrrtstMapConfig(ScrrtstRepository scrrtstRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ScrrtstLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ScrrtstLoader scrrtstLoader = new ScrrtstLoader(scrrtstRepository);
        mapStoreConfig.setImplementation(scrrtstLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SCRRTST);
        mapConfig.setName(CacheClient.SCRRTST);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    private MapConfig getShrgrdeMapConfig(ShrgrdeRepository shrgrdeRepository) {
        MapStoreConfig mapStoreConfig = new MapStoreConfig();
        mapStoreConfig.setEnabled(true);
        mapStoreConfig.setClassName("com.edunav.catalog.course.loader.ShrgrdeLoader");
        mapStoreConfig.setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER);

        ShrgrdeLoader shrgrdeLoader = new ShrgrdeLoader(shrgrdeRepository);
        mapStoreConfig.setImplementation(shrgrdeLoader);

        MapConfig mapConfig = new MapConfig(CacheClient.SHRGRDE);
        mapConfig.setName(CacheClient.SHRGRDE);
        mapConfig.setMapStoreConfig(mapStoreConfig);
        return mapConfig;
    }

    @Bean
    public SpringManagedContext managedContext() {
        return new SpringManagedContext();
    }
}
