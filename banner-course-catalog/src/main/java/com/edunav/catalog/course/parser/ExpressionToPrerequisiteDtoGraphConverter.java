package com.edunav.catalog.course.parser;

import com.edunav.catalog.dto.requisite.PrerequisiteDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static java.util.stream.Collectors.joining;

public class ExpressionToPrerequisiteDtoGraphConverter {

    public static final String LEFT_PAREN = "(";
    public static final String RIGHT_PAREN = ")";
    private static final Logger log = LoggerFactory.getLogger(ExpressionToPrerequisiteDtoGraphConverter.class);


    public PrerequisiteDto convert(List<Object> sourceElements) {
        List<Object> elements = new ArrayList<>(sourceElements.size() + 2);
        elements.addAll(sourceElements);
        Stack<Object> root = new Stack<>();
        Stack<Object> connections = new Stack<>();
        elements.add(0, LEFT_PAREN);
        elements.add(RIGHT_PAREN);
        int leftParen = 0;
        for (Object element : elements) {
            if (isLeftParenthesis(element)) {
                connections.add(element);
                leftParen++;
                continue;
            }
            if (isRightParenthesis(element)) {
                if (leftParen == 0) {
                    log.error("Expression parsed with errors, more right parentheses than left ones: {}",
                              sourceElements.stream().map(Object::toString).collect(joining(" ")));
                    break;
                }
                while (true) {
                    if (connections.size() == 0) {
                        break;
                    }
                    Object connector = connections.pop();
                    if (isLeftParenthesis(connector)) {
                        leftParen--;
                        break;
                    }
                    Object[] values = new Object[2];
                    for (int i = 0; i < 2; i++) {
                        if (!root.isEmpty()) {
                            values[i] = root.pop();
                        }
                    }
                    root.push(compute(connector, values));
                }
            } else {
                if (isConnection(element)) {
                    connections.add(element);
                    continue;
                }
                root.push(element);
            }
        }
        if (root.size() > 1) {
            log.error("Expression parsed with errors, more than one element remains: {}", root);
        }
        Object result = root.size() == 0 ? null : root.pop();
        if (result instanceof PrerequisiteDto) {
            return (PrerequisiteDto) result;
        } else {
            return PrerequisiteDto.buildAnd(result);
        }
    }

    private Object compute(Object connector, Object... elements) {
        boolean isOr = isOr(connector);
        PrerequisiteDto pr = isOr ? PrerequisiteDto.buildOr() : PrerequisiteDto.buildAnd();
        List<Object> prList = pr.getList();
        for (Object el : elements) {
            if (el == null) {
                continue;
            }
            if (el instanceof PrerequisiteDto && ((PrerequisiteDto) el).isOrCondition() == isOr) {
                prList.addAll(((PrerequisiteDto) el).getList());
            } else {
                prList.add(el);
            }
        }
        return pr;
    }


    private boolean isRightParenthesis(Object element) {
        return RIGHT_PAREN.equals(element);
    }

    private boolean isLeftParenthesis(Object element) {
        return LEFT_PAREN.equals(element);
    }

    private boolean isConnection(Object element) {
        return isAnd(element) || isOr(element);
    }

    private boolean isAnd(Object element) {
        return "+".equals(element);
    }

    private boolean isOr(Object element) {
        return "-".equals(element);
    }
}
