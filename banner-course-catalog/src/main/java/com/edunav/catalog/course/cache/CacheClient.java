package com.edunav.catalog.course.cache;

import com.edunav.catalog.course.repository.CourseRepository;
import com.edunav.catalog.dto.key.ScbcrkyKey;
import com.edunav.catalog.dto.key.ScbcrseKey;
import com.edunav.catalog.dto.key.ScbdescKey;
import com.edunav.catalog.dto.key.ScrattrKey;
import com.edunav.catalog.dto.key.ScrcorqKey;
import com.edunav.catalog.dto.key.ScreqivKey;
import com.edunav.catalog.dto.key.ScrlevlKey;
import com.edunav.catalog.dto.key.ScrmexcKey;
import com.edunav.catalog.dto.key.ScrrtstKey;
import com.edunav.catalog.dto.key.ScrtextKey;
import com.edunav.catalog.dto.key.StvtermKey;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.IMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

@Component
public class CacheClient {

    public static final String SCBCRSE = "SCBCRSE";
    public static final String SCRTEXT = "SCRTEXT";
    public static final String SCBDESC = "SCBDESC";
    public static final String SCBCRKY = "SCBCRKY";
    public static final String STVTERM = "STVTERM";
    public static final String SCRATTR = "SCRATTR";
    public static final String SCRCORQ = "SCRCORQ";
    public static final String SCREQIV = "SCREQIV";
    public static final String SCRMEXC = "SCRMEXC";
    public static final String SCRLEVL = "SCRLEVL";
    public static final String SCRRTST = "SCRRTST";
    public static final String SHRGRDE = "SHRGRDE";

    private final Logger logger = LoggerFactory.getLogger(CacheClient.class);

    private final HazelcastInstance hazelcastInstance;

    @Autowired
    public CacheClient(HazelcastInstance hazelcastInstance) {
        this.hazelcastInstance = hazelcastInstance;
    }

    public List<Map<String, Object>> getAllScbcrse() {
        IMap<ScbcrseKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCBCRSE);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }

    public List<Map<String, Object>> getAllScrtext() {
        IMap<ScrtextKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCRTEXT);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }

    public List<Map<String, Object>> getAllScbdesc() {
        IMap<ScbdescKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCBDESC);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }

    public List<Map<String, Object>> getAllScbcrky() {
        IMap<ScbcrkyKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCBCRKY);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }

    public List<Map<String, Object>> getAllStvterm() {
        IMap<StvtermKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(STVTERM);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }

    public List<ScrattrKey> getAllScrattr() {
        IMap<ScrattrKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCRATTR);
        return new ArrayList<>(hazelcastInstanceMap.keySet());
    }

    public List<ScrcorqKey> getAllScrcorq() {
        IMap<ScrcorqKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCRCORQ);
        return new ArrayList<>(hazelcastInstanceMap.keySet());
    }

    public List<ScreqivKey> getAllScreqiv() {
        IMap<ScreqivKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCREQIV);
        return new ArrayList<>(hazelcastInstanceMap.keySet());
    }

    public List<ScrmexcKey> getAllScrmexc() {
        IMap<ScrmexcKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCRMEXC);
        return new ArrayList<>(hazelcastInstanceMap.keySet());
    }

    public List<ScrlevlKey> getAllScrlevl() {
        IMap<ScrlevlKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCRLEVL);
        return new ArrayList<>(hazelcastInstanceMap.keySet());
    }

    public List<Map<String, Object>> getAllScrrtst() {
        IMap<ScrrtstKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCRRTST);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }


    public Map<String, Object> getScbcrseByKey(ScbcrseKey scbcrseKey) {
        IMap<ScbcrseKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SCBCRSE);
        return hazelcastInstanceMap.get(scbcrseKey);
    }

    public List<Map<String, Object>> getAllShrgrde() {
        IMap<StvtermKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap(SHRGRDE);
        return new ArrayList<>(hazelcastInstanceMap.values());
    }

    public List<Map<String, Object>> getAllTest() {
        IMap<ScbcrseKey, Map<String, Object>> hazelcastInstanceMap = hazelcastInstance.getMap("TEST");
        return new ArrayList<>(hazelcastInstanceMap.values());
    }
}