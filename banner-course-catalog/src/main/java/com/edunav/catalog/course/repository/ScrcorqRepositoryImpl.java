package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScrcorqKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScrcorqRepositoryImpl extends BaseRepositoryImpl<ScrcorqKey> implements ScrcorqRepository<ScrcorqKey> {

    @Autowired
    public ScrcorqRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCRCORQ";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCRCORQ_SUBJ_CODE", "SCRCORQ_CRSE_NUMB", "SCRCORQ_EFF_TERM", "SCRCORQ_SUBJ_CODE_CORQ", "SCRCORQ_CRSE_NUMB_CORQ");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCRCORQ_SUBJ_CODE", "SCRCORQ_CRSE_NUMB", "SCRCORQ_EFF_TERM", "SCRCORQ_SUBJ_CODE_CORQ", "SCRCORQ_CRSE_NUMB_CORQ");
    }

    @Override
    protected Function<Map<String, Object>, ScrcorqKey> getKeyMapper() {
        return MapperFunctions.SCRCORQ_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScrcorqKey key) {
        return String.format("select %s from %s where SCRCORQ_SUBJ_CODE=%s and SCRCORQ_CRSE_NUMB=%s and SCRCORQ_EFF_TERM=%s and SCRCORQ_SUBJ_CODE_CORQ=%s and SCRCORQ_CRSE_NUMB_CORQ=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getSubjCodeCorq(), key.getCrseNumbCorq());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScrcorqKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCRCORQ_SUBJ_CODE, SCRCORQ_CRSE_NUMB, SCRCORQ_EFF_TERM, coalesce(SCRCORQ_SUBJ_CODE_CORQ, 'null'), coalesce(SCRCORQ_CRSE_NUMB_CORQ, 'null')) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScrcorqKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScrcorqKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getSubjCodeCorq(), key.getCrseNumbCorq()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
