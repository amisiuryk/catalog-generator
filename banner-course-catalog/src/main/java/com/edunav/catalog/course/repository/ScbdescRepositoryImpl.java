package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScbdescKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScbdescRepositoryImpl extends BaseRepositoryImpl<ScbdescKey> implements ScbdescRepository<ScbdescKey> {

    @Autowired
    public ScbdescRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCBDESC";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCBDESC_SUBJ_CODE", "SCBDESC_CRSE_NUMB", "SCBDESC_TERM_CODE_EFF", "SCBDESC_TEXT_NARRATIVE", "SCBDESC_TERM_CODE_END");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCBDESC_SUBJ_CODE", "SCBDESC_CRSE_NUMB", "SCBDESC_TERM_CODE_EFF", "SCBDESC_TERM_CODE_END");
    }

    @Override
    protected Function<Map<String, Object>, ScbdescKey> getKeyMapper() {
        return MapperFunctions.SCBDESC_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScbdescKey key) {
        return String.format("select %s from %s where SCBDESC_SUBJ_CODE=%s and SCBDESC_CRSE_NUMB=%s and SCBDESC_TERM_CODE_EFF=%s and SCBDESC_TERM_CODE_END=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getTermCodeEff(), key.getTermCodeEnd());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScbdescKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCBDESC_SUBJ_CODE, SCBDESC_CRSE_NUMB, SCBDESC_TERM_CODE_EFF, coalesce(SCBDESC_TERM_CODE_END, 'null')) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScbdescKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScbdescKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getTermCodeEff(), key.getTermCodeEnd()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
