package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScrcorqRepository;
import com.edunav.catalog.dto.key.ScrcorqKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScrcorqLoader extends BaseLoader<ScrcorqKey> {

    public ScrcorqLoader(ScrcorqRepository<ScrcorqKey> scrcorqRepository) {
        super(scrcorqRepository);
    }
}
