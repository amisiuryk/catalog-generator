package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScreqivRepository;
import com.edunav.catalog.dto.key.ScreqivKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScreqivLoader extends BaseLoader<ScreqivKey> {

    public ScreqivLoader(ScreqivRepository<ScreqivKey> screqivRepository) {
        super(screqivRepository);
    }
}
