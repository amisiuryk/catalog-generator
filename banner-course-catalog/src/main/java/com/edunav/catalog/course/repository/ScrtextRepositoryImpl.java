package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScrtextKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScrtextRepositoryImpl extends BaseRepositoryImpl<ScrtextKey> implements ScrtextRepository<ScrtextKey> {

    @Autowired
    public ScrtextRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCRTEXT";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCRTEXT_SURROGATE_ID", "SCRTEXT_SUBJ_CODE", "SCRTEXT_CRSE_NUMB", "SCRTEXT_EFF_TERM", "SCRTEXT_TEXT_CODE", "SCRTEXT_SEQNO",
                "SCRTEXT_TEXT", "SCRTEXT_ACTIVITY_DATE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCRTEXT_SURROGATE_ID");
    }

    @Override
    protected Function<Map<String, Object>, ScrtextKey> getKeyMapper() {
        return MapperFunctions.SCRTEXT_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScrtextKey key) {
        return String.format("select %s from %s where SCRTEXT_SURROGATE_ID=%s", String.join(", ", getAllColumns()), getTable(), key.getId());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScrtextKey> keys) {
        String baseQuery = String.format("Select %s from %s where SCRTEXT_SURROGATE_ID IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScrtextKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScrtextKey key = iterator.next();
            queryBuilder.append(key.getId());
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
