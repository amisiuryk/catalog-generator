package com.edunav.catalog.course.converter;

import java.math.BigDecimal;

public class BaseConverter {

    protected Double getDouble(BigDecimal bigDecimal) {
        return bigDecimal == null ? null : bigDecimal.doubleValue();
    }

    protected Float getFloat(BigDecimal bigDecimal) {
        return bigDecimal == null ? null : bigDecimal.floatValue();
    }

    protected Integer getInteger(BigDecimal bigDecimal) {
        return bigDecimal == null ? null : bigDecimal.intValue();
    }
}
