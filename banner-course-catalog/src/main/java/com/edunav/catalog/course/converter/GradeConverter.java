package com.edunav.catalog.course.converter;

import com.edunav.catalog.dto.additional.Grade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class GradeConverter extends BaseConverter {

    private final Logger logger = LoggerFactory.getLogger(GradeConverter.class);

    public List<Grade> createGrades(List<Map<String, Object>> allShrgrde) {
        List<Grade> grades = new ArrayList<>();
        for (Map<String, Object> rowGrade : allShrgrde) {
            Grade grade = new Grade();
            grade.setAbbrev((String) rowGrade.get("SHRGRDE_ABBREV"));
            grade.setIsAttempted(rowGrade.get("SHRGRDE_ATTEMPTED_IND").equals("Y"));
            grade.setIsCompleted(rowGrade.get("SHRGRDE_COMPLETED_IND").equals("Y"));
            grade.setIsGpa(rowGrade.get("SHRGRDE_GPA_IND").equals("Y"));
            grade.setGrade((String) rowGrade.get("SHRGRDE_CODE"));
            grade.setLevel((String) rowGrade.get("SHRGRDE_LEVL_CODE"));
            grade.setIsPassed(rowGrade.get("SHRGRDE_PASSED_IND").equals("Y"));
            grade.setIsPassed(rowGrade.get("SHRGRDE_PASSED_IND").equals("Y"));
            grade.setQualityPoints(getDouble((BigDecimal) rowGrade.get("SHRGRDE_QUALITY_POINTS")));
            grade.setTerm((String) rowGrade.get("SHRGRDE_TERM_CODE_EFFECTIVE"));
            Integer numValue = getInteger((BigDecimal) rowGrade.get("SHRGRDE_NUMERIC_VALUE"));
            grade.setValue(numValue);
            grade.setId(String.format("%s:%s:%s", grade.getGrade(), grade.getLevel(), grade.getTerm()));
            grades.add(grade);
        }
        return grades;
    }
}
