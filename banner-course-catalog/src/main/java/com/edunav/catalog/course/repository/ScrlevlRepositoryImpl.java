package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScrlevlKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScrlevlRepositoryImpl extends BaseRepositoryImpl<ScrlevlKey> implements ScrlevlRepository<ScrlevlKey> {

    @Autowired
    public ScrlevlRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCRLEVL";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCRLEVL_SUBJ_CODE", "SCRLEVL_CRSE_NUMB", "SCRLEVL_EFF_TERM", "SCRLEVL_LEVL_CODE");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCRLEVL_SUBJ_CODE", "SCRLEVL_CRSE_NUMB", "SCRLEVL_EFF_TERM", "SCRLEVL_LEVL_CODE");
    }

    @Override
    protected Function<Map<String, Object>, ScrlevlKey> getKeyMapper() {
        return MapperFunctions.SCRLEVL_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScrlevlKey key) {
        return String.format("select %s from %s where SCRLEVL_SUBJ_CODE=%s and SCRLEVL_CRSE_NUMB=%s and SCRLEVL_EFF_TERM=%s and SCRLEVL_LEVL_CODE=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getLevlCode());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScrlevlKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCRLEVL_SUBJ_CODE, SCRLEVL_CRSE_NUMB, SCRLEVL_EFF_TERM, SCRLEVL_LEVL_CODE) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScrlevlKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScrlevlKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getLevlCode()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
