package com.edunav.catalog.course.service;

import com.edunav.catalog.dto.course.CourseDto;

import java.util.List;
import java.util.Map;

public interface CourseService {
    List<Map<String, Object>> getAllTest();

    List<CourseDto> getAllCourses();

    Map<String, Object> getCourse(String p1, String p2, String p3);
}
