package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScbcrseRepository;
import com.edunav.catalog.dto.key.ScbcrseKey;
import com.hazelcast.map.MapLoader;
import com.hazelcast.spring.context.SpringAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Collection;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@SpringAware
//@Component
//public class ScbcrseLoader implements ApplicationContextAware, MapLoader<ScbcrseKey, Map<String, Object>> {
public class ScbcrseLoader extends BaseLoader<ScbcrseKey> {

    public ScbcrseLoader(ScbcrseRepository<ScbcrseKey> scbcrseRepository) {
        super(scbcrseRepository);
    }


    //    ---------------------- first impl
//
////    @Autowired
//    private ApplicationContext applicationContext;
//    @Autowired
//    private ScbcrseRepository scbcrseRepository;
//    private String test;
//
//    private final Logger logger = LoggerFactory.getLogger(ScbcrseLoader.class);
//
////    @Autowired
////    public ScbcrseLoader(ScbcrseRepositoryImpl scbcrseRepository) {
////        this.scbcrseRepository = scbcrseRepository;
////    }
//
//
//    public void setScbcrseRepository(ScbcrseRepository scbcrseRepository) {
//        this.scbcrseRepository = scbcrseRepository;
//    }
//
//    public void setTest(String test) {
//        this.test = test;
//    }
//
//    @Override
//    public Map<String, Object> load(ScbcrseKey key) {
//        logger.info("Load by key {}", key);
//        return scbcrseRepository.findByKey(key);
//    }
//
//    @Override
//    public Map<ScbcrseKey, Map<String, Object>> loadAll(Collection<ScbcrseKey> keys) {
////        List<Map<String, Object>> allScbcrse = scbcrseRepository.findAllScbcrse();
////        Map<ScbcrseKey, Map<String, Object>> allScbcrseByKey = allScbcrse.stream()
////                .collect(toMap(MapperFunctions.SCBCRSE_KEY_MAPPER, Function.identity()));
////        Map<ScbcrseKey, Map<String, Object>> result = new HashMap<>();
////        keys.forEach(it -> {
////            Map<String, Object> map;
////            if (allScbcrseByKey.containsKey(it)) {
////                map = allScbcrseByKey.get(it);
////            } else {
////                map = scbcrseRepository.findByKey(it);
////            }
////            result.put(it, map);
////        });
////        logger.info("Load all");
////        return result;
//        logger.info("Load all");
//        return scbcrseRepository.findByKeys(keys);
//    }
//
//    @Override
//    public Iterable<ScbcrseKey> loadAllKeys() {
//        logger.info("Load all keys");
//        return scbcrseRepository.findAllKeys();
//    }
//
//
//
////    public ScbcrseRepositoryImpl getScbcrseRepository() {
////        if (this.scbcrseRepository == null) {
////            this.scbcrseRepository = applicationContext.getBean(ScbcrseRepositoryImpl.class);
////        }
////        return scbcrseRepository;
////    }
////
//    @Override
//    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
//        this.applicationContext = applicationContext;
//    }
//
////    @PostConstruct
////    public void init() {
////
////    }
}
