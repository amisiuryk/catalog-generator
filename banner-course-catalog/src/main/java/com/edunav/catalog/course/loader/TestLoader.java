package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.BaseRepository;
import com.edunav.catalog.course.repository.ScbcrseRepository;
import com.edunav.catalog.dto.key.ScbcrseKey;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.map.MapLoader;
import com.hazelcast.map.MapLoaderLifecycleSupport;
import com.hazelcast.spring.context.SpringAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Map;
import java.util.Properties;

@SpringAware
//@Component
//public class ScbcrseLoader implements ApplicationContextAware, MapLoader<ScbcrseKey, Map<String, Object>> {
public class TestLoader implements MapLoader<ScbcrseKey, Map<String, Object>>, MapLoaderLifecycleSupport {

    @Autowired
    private ScbcrseRepository<ScbcrseKey> baseRepository;

    private final Logger logger = LoggerFactory.getLogger(BaseLoader.class);

//    public BaseLoader(BaseRepository<ScbcrseKey> baseRepository) {
//        this.baseRepository = baseRepository;
//    }

    @Override
    public Map<String, Object> load(ScbcrseKey key) {
        logger.info("Load by key {}", key);
        return baseRepository.findByKey(key);
    }

    @Override
    public Map<ScbcrseKey, Map<String, Object>> loadAll(Collection<ScbcrseKey> keys) {
        logger.info("Load all");
        return baseRepository.findByKeys(keys);
    }

    @Override
    public Iterable<ScbcrseKey> loadAllKeys() {
        logger.info("Load all keys");
        return baseRepository.findAllKeys();
    }

    @Override
    public void init(HazelcastInstance hazelcastInstance, Properties properties, String mapName) {
        hazelcastInstance.getConfig().getManagedContext().initialize(this);
    }

    @Override
    public void destroy() {

    }
}
