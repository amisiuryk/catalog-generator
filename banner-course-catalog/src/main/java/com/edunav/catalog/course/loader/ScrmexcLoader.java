package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScrmexcRepository;
import com.edunav.catalog.dto.key.ScrmexcKey;
import com.hazelcast.spring.context.SpringAware;

@SpringAware
public class ScrmexcLoader extends BaseLoader<ScrmexcKey> {

    public ScrmexcLoader(ScrmexcRepository<ScrmexcKey> scrmexcRepository) {
        super(scrmexcRepository);
    }
}
