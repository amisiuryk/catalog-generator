package com.edunav.catalog.course.repository;

import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.key.ScreqivKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Repository
public class ScreqivRepositoryImpl extends BaseRepositoryImpl<ScreqivKey> implements ScreqivRepository<ScreqivKey> {

    @Autowired
    public ScreqivRepositoryImpl(JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate);
    }

    @Override
    protected String getTable() {
        return "SATURN.SCREQIV";
    }

    @Override
    protected List<String> getAllColumns() {
        return List.of("SCREQIV_SUBJ_CODE", "SCREQIV_CRSE_NUMB", "SCREQIV_EFF_TERM", "SCREQIV_SUBJ_CODE_EQIV", "SCREQIV_CRSE_NUMB_EQIV", "SCREQIV_START_TERM");
    }

    @Override
    protected List<String> getKeyColumns() {
        return List.of("SCREQIV_SUBJ_CODE", "SCREQIV_CRSE_NUMB", "SCREQIV_EFF_TERM", "SCREQIV_SUBJ_CODE_EQIV", "SCREQIV_CRSE_NUMB_EQIV", "SCREQIV_START_TERM");
    }

    @Override
    protected Function<Map<String, Object>, ScreqivKey> getKeyMapper() {
        return MapperFunctions.SCREQIV_KEY_MAPPER;
    }

    @Override
    protected String getOneByKeyQuery(ScreqivKey key) {
        return String.format("select %s from %s where SCREQIV_SUBJ_CODE=%s and SCREQIV_CRSE_NUMB=%s and SCREQIV_EFF_TERM=%s and SCREQIV_SUBJ_CODE_EQIV=%s and SCREQIV_CRSE_NUMB_EQIV=%s and SCREQIV_START_TERM=%s",
                String.join(", ", getAllColumns()), getTable(), key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getSubjCodeEqiv(), key.getCrseNumbEqiv(), key.getStartTerm());
    }

    @Override
    protected String getListByKeysQuery(Collection<ScreqivKey> keys) {
        String baseQuery = String.format("Select %s from %s where (SCREQIV_SUBJ_CODE, SCREQIV_CRSE_NUMB, SCREQIV_EFF_TERM, " +
                "coalesce(SCREQIV_SUBJ_CODE_EQIV, 'null'), coalesce(SCREQIV_CRSE_NUMB_EQIV, 'null'), coalesce(SCREQIV_START_TERM, 'null')) IN (", String.join(", ", getAllColumns()), getTable());
        StringBuilder queryBuilder = new StringBuilder(baseQuery);
        Iterator<ScreqivKey> iterator = keys.iterator();
        while (iterator.hasNext()) {
            ScreqivKey key = iterator.next();
            queryBuilder.append(String.format("('%s','%s','%s','%s','%s','%s')", key.getSubjCode(), key.getCrseNumb(), key.getEffTerm(), key.getSubjCodeEqiv(), key.getCrseNumbEqiv(), key.getStartTerm()));
            if (iterator.hasNext()) {
                queryBuilder.append(", ");
            } else {
                queryBuilder.append(")");
            }
        }
        return queryBuilder.toString();
    }
}
