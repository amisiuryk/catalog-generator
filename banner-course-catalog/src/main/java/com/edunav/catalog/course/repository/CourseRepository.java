package com.edunav.catalog.course.repository;

import java.util.List;
import java.util.Map;

public interface CourseRepository {
    List<Map<String, Object>> getAllCourses();
}
