package com.edunav.catalog.course.parser;


import com.edunav.catalog.dto.common.BaseAreaPrerequisite;
import com.edunav.catalog.dto.common.CourseRange;
import com.edunav.catalog.dto.prerequisite.PrerequisiteElement;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.edunav.catalog.course.parser.PrerequisiteType.COURSE;
import static com.edunav.catalog.course.parser.PrerequisiteType.PLACEMENT;


public class DatabaseRowBasedComposer<T extends BaseAreaPrerequisite> implements ExpressionComposer<Iterable<T>> {

    @Override
    public List<Object> composeExpression(Iterable<T> courses) {
        List<Object> o = new ArrayList<>();
        for (T course : courses) {
            o.addAll(composeExpression(course));
        }
        return o;
    }

    private List<?> composeExpression(T course) {
        String leftParen = course.getLeftParen();
        String prerequisitesCode = course.getPrerequisitesCourseId();
        String relation = course.getPrerequisitesType();
        String rightParent = course.getRightParent();
        String tescCode = course.getTest();
        String testScore = course.getTestScore();
        String maxTestScore = course.getMaxTestScore();
        CourseRange range = course.getRange();
        boolean concurrency = course.getConcurrency();
        String grade = course.getGrade();
        List<Object> objects = new ArrayList<>();
        parseRelation(relation).ifPresent(objects::add);
        parseParentheses(leftParen).ifPresent(objects::add);
        parsePrerequisiteElement(prerequisitesCode, grade, concurrency, range).ifPresent(objects::add);
        parsePlacement(tescCode, testScore, maxTestScore).ifPresent(objects::add);
        parseParentheses(rightParent).ifPresent(objects::add);
        return objects;
    }

    private Optional<PrerequisiteElement> parsePlacement(String tescCode, String testScore, String maxTestScore) {
        if (StringUtils.isNotBlank(tescCode)) {
            PrerequisiteElement prerequisiteElement = new PrerequisiteElement();
            prerequisiteElement.setId(tescCode);
            prerequisiteElement.setType(PLACEMENT.getType());
            prerequisiteElement.setGrade(testScore);
            prerequisiteElement.setMaxGrade(maxTestScore);
            return Optional.of(prerequisiteElement);
        }
        return Optional.empty();
    }

    private Optional<PrerequisiteElement> parsePrerequisiteElement(String prerequisitesCode, String grade, boolean concurrency, CourseRange courseRange) {
        if (StringUtils.isNotBlank(prerequisitesCode)) {
            PrerequisiteElement prerequisiteElement = new PrerequisiteElement();
            prerequisiteElement.setId(prerequisitesCode);
            prerequisiteElement.setType(COURSE.getType());
            prerequisiteElement.setGrade(grade);
            prerequisiteElement.setConcurrency(concurrency);
            prerequisiteElement.setRange(courseRange);
            return Optional.of(prerequisiteElement);
        }
        return Optional.empty();
    }

    private Optional<String> parseParentheses(String paren) {
        if (StringUtils.isNotBlank(paren)) {
            return Optional.of(paren);
        }
        return Optional.empty();
    }

    private Optional<String> parseRelation(String relation) {
        if (StringUtils.isNotBlank(relation)) {
            return Optional.ofNullable(formatRelation(relation));
        }
        return Optional.empty();
    }

    private static String formatRelation(String relation) {
        switch (relation) {
            case "A":
                return "+";
            case "O":
                return "-";
        }
        return null;
    }

}
