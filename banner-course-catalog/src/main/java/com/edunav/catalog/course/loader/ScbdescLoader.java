package com.edunav.catalog.course.loader;

import com.edunav.catalog.course.repository.ScbdescRepository;
import com.edunav.catalog.dto.key.ScbcrseKey;
import com.hazelcast.spring.context.SpringAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SpringAware
public class ScbdescLoader extends BaseLoader<ScbdescLoader> {

    private final Logger logger = LoggerFactory.getLogger(ScbdescLoader.class);

    public ScbdescLoader(ScbdescRepository<ScbdescLoader> scbdescRepository) {
        super(scbdescRepository);
    }
}
