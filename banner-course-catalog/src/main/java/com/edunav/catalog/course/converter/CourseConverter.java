package com.edunav.catalog.course.converter;

import com.edunav.catalog.course.parser.DatabaseRowBasedComposer;
import com.edunav.catalog.course.parser.ExpressionToPrerequisiteDtoGraphConverter;
import com.edunav.catalog.course.util.MapperFunctions;
import com.edunav.catalog.dto.additional.PrerequisiteSourceTypeDto;
import com.edunav.catalog.dto.course.CourseDto;
import com.edunav.catalog.dto.course.CoursePrerequisite;
import com.edunav.catalog.dto.course.DepartmentDto;
import com.edunav.catalog.dto.key.CrseSimpleKey;
import com.edunav.catalog.dto.key.ScbcrseKey;
import com.edunav.catalog.dto.key.ScrattrKey;
import com.edunav.catalog.dto.key.ScrcorqKey;
import com.edunav.catalog.dto.key.ScreqivKey;
import com.edunav.catalog.dto.key.ScrlevlKey;
import com.edunav.catalog.dto.key.ScrmexcKey;
import com.edunav.catalog.dto.prerequisite.PrerequisiteElement;
import com.edunav.catalog.dto.prerequisite.PrerequisiteExclusionModel;
import com.edunav.catalog.dto.requisite.BaseRequisite;
import com.edunav.catalog.dto.requisite.CourseSet;
import com.edunav.catalog.dto.requisite.CoursesRequisite;
import com.edunav.catalog.dto.requisite.CoursesRequisiteExclusion;
import com.edunav.catalog.dto.requisite.MinGradeSet;
import com.edunav.catalog.dto.requisite.PlacementRequisite;
import com.edunav.catalog.dto.requisite.PlacementRequisiteItem;
import com.edunav.catalog.dto.requisite.PrerequisiteDto;
import com.edunav.catalog.dto.requisite.Requisite;
import com.edunav.catalog.dto.requisite.ScoreRequisite;
import com.edunav.catalog.dto.requisite.ScoreRequisiteItem;
import com.edunav.catalog.dto.requisite.StudentGroupRequisite;
import com.edunav.catalog.dto.requisite.StudentGroupRequisiteItem;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.edunav.catalog.course.util.MapperFunctions.ID_FORMATTER;
import static com.edunav.catalog.course.util.MapperFunctions.SCBCRSE_KEY_MAPPER;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

@Service
public class CourseConverter extends BaseConverter {

    private final Logger logger = LoggerFactory.getLogger(CourseConverter.class);

    private DatabaseRowBasedComposer databaseRowBasedComposer;

    @Autowired
    public CourseConverter() {
        this.databaseRowBasedComposer = new DatabaseRowBasedComposer();
    }

    public void createPrerequisite(Map<ScbcrseKey, CourseDto> baseCourses, List<Map<String, Object>> allScrrtst, Map<String, Double> gradesMap) {
        Map<ScbcrseKey, List<Map<String, Object>>> scrrtstByCrse = allScrrtst.stream()
                .collect(groupingBy(it -> new ScbcrseKey((String) it.get("SCRRTST_SUBJ_CODE"), (String) it.get("SCRRTST_CRSE_NUMB"), (String) it.get("SCRRTST_TERM_CODE_EFF"))));

        Map<ScbcrseKey, TreeSet<CoursePrerequisite>> scbToPrerequisites = new HashMap<>();

        for (Map.Entry<ScbcrseKey, List<Map<String, Object>>> entry : scrrtstByCrse.entrySet()) {
            ScbcrseKey scbcrseKey = entry.getKey();
            CourseDto courseDto = baseCourses.get(scbcrseKey);
            for (Map<String, Object> prereq : entry.getValue()) {
                CoursePrerequisite coursePrerequisite = new CoursePrerequisite();
                coursePrerequisite.setId(String.format(ID_FORMATTER, scbcrseKey.getSubjCode(), scbcrseKey.getCrseNumb()));
                coursePrerequisite.setTest((String) prereq.get("SCRRTST_TESC_CODE"));
                coursePrerequisite.setTestScore((String) prereq.get("SCRRTST_TEST_SCORE"));
                coursePrerequisite.setGrade((String) prereq.get("SCRRTST_MIN_GRDE"));

                String prerequisiteCourseSubj = (String) prereq.get("SCRRTST_SUBJ_CODE_PREQ");
                if (prerequisiteCourseSubj != null) {
                    coursePrerequisite.setPrerequisitesCourseId(prerequisiteCourseSubj + " " + prereq.get("SCRRTST_CRSE_NUMB_PREQ"));
                }
                Double prerequisiteSeqno = getDouble((BigDecimal) prereq.get("SCRRTST_SEQNO"));
                if (prerequisiteSeqno != null) {
                    coursePrerequisite.setPrerequisiteSeqno(prerequisiteSeqno);
                }
                coursePrerequisite.setPrerequisiteRelation((String) prereq.get("SCRRTST_CONNECTOR"));
                coursePrerequisite.setLeftParen((String) prereq.get("SCRRTST_LPAREN"));
                coursePrerequisite.setRightParent((String) prereq.get("SCRRTST_RPAREN"));
                String concurrency = (String) prereq.get("SCRRTST_CONCURRENCY_IND");
                if (concurrency != null && concurrency.equalsIgnoreCase("Y")) {
                    coursePrerequisite.setConcurrency(true);
                }

                scbToPrerequisites.computeIfAbsent(scbcrseKey, it -> new TreeSet<>()).add(coursePrerequisite);
            }
        }

        for (Map.Entry<ScbcrseKey, TreeSet<CoursePrerequisite>> entry : scbToPrerequisites.entrySet()) {
            CoursePrerequisite coursePrerequisite = entry.getValue().first();
            if (prerequisiteCondition(coursePrerequisite)) {
                List<Object> o = databaseRowBasedComposer.composeExpression(entry.getValue());
                logger.debug("Converting: {} prerequisites: {}", coursePrerequisite.getId(), o.stream().map(Object::toString).collect(joining()));
                ExpressionToPrerequisiteDtoGraphConverter converter = new ExpressionToPrerequisiteDtoGraphConverter();
                PrerequisiteDto prerequisiteDto = converter.convert(o);
                CourseDto courseDto = baseCourses.get(entry.getKey());
                if (courseDto != null) {
                    courseDto.setPrerequisites(prerequisiteDto);
                    courseDto.setPrerequisiteSourceType(PrerequisiteSourceTypeDto.DATA_SET);
                    Requisite requisite = convertPrerequisitesToRequisites(prerequisiteDto, gradesMap);
                    courseDto.setRequisite(requisite);
                } else {
                    logger.debug("no course for {}", entry.getKey());
                }
            }
        }
    }

    private Requisite convertPrerequisitesToRequisites(PrerequisiteDto prerequisites, Map<String, Double> gradesMap) {
        Requisite requisite = new Requisite();
        List<BaseRequisite> subRequisites = null;
        if (prerequisites.getOr() != null) {
            requisite.setConnector("or");
            subRequisites = createInnerRequisites(prerequisites.getOr(), gradesMap);
        }

        if (prerequisites.getAnd() != null) {
            requisite.setConnector("and");
            subRequisites = createInnerRequisites(prerequisites.getAnd(), gradesMap);
        }

//        copyExtraFields(prerequisites, requisite);
        requisite.setRequisites(subRequisites);

        return requisite;
    }

    private List<BaseRequisite> createInnerRequisites(List<Object> prerequisites, Map<String, Double> gradesMap) {
        List<BaseRequisite> requisites = new ArrayList<>();
        if (isNull(prerequisites)) {
            return requisites;
        }
        for (Object prerequisite : prerequisites) {
            Requisite requisite = new Requisite();

            if (prerequisite instanceof PrerequisiteDto) {

                PrerequisiteDto prerequisiteDto = (PrerequisiteDto) prerequisite;
                List<Object> listOrAnd;
                if (prerequisiteDto.getOr() != null) {
                    requisite.setConnector("or");
                    listOrAnd = prerequisiteDto.getOr();
                } else {
                    requisite.setConnector("and");
                    listOrAnd = prerequisiteDto.getAnd();
                }
                if (!org.apache.commons.lang3.StringUtils.isEmpty(prerequisiteDto.getRuleCode())) {
                    requisite.setRuleCode(prerequisiteDto.getRuleCode());
                }
                if (!org.apache.commons.lang3.StringUtils.isEmpty(prerequisiteDto.getAreaCode())) {
                    requisite.setAreaCode(prerequisiteDto.getAreaCode());
                }
                if (!org.apache.commons.lang3.StringUtils.isEmpty(prerequisiteDto.getGroupCode())) {
                    requisite.setGroupCode(prerequisiteDto.getGroupCode());
                }
                copyExtraFields((PrerequisiteDto) prerequisite, requisite);
//                requisite.setRequiredNumberOfConditions(((PrerequisiteDto) prerequisite).getRequiredNumberOfConditions());
//                requisite.setConnectorMaxCond(prerequisiteDto.getConnectorMaxCond());
//                requisite.setConnectorMax(prerequisiteDto.getConnectorMax());
//                requisite.setConnectorCond(prerequisiteDto.getConnectorCond());
//                requisite.setConnectorReq(prerequisiteDto.getConnectorReq());

                requisite.setRequisites(createInnerRequisites(listOrAnd, gradesMap));
            }

            if (prerequisite instanceof PrerequisiteElement) {
                PrerequisiteElement prerequisiteElement = (PrerequisiteElement) prerequisite;
                if (prerequisiteElement.getType().equals("course")) {
                    CoursesRequisite coursesRequisite = new CoursesRequisite();
                    List<CourseSet> courses = new ArrayList<>();
                    CourseSet courseSet = new CourseSet();
                    courseSet.setRange(prerequisiteElement.getRange());
                    courseSet.setCourseSet(prerequisiteElement.getId());

                    String courseAttrCode = prerequisiteElement.getCourseAttrCode();
                    if (StringUtils.isNotEmpty(courseAttrCode)) {
                        List<Object> attribute = List.of("attributes", "=", courseAttrCode);
                        ArrayList<Object> objects = new ArrayList<>();
                        objects.add(attribute);
                        courseSet.setQualifiers(objects);
                    }
                    String courseSubject = prerequisiteElement.getSubject();
                    if (StringUtils.isNotEmpty(courseSubject)) {
                        List<Object> courseSubjectQualifier = List.of("subject", "=", courseSubject);
                        ArrayList<Object> objects = isNull(courseSet.getQualifiers()) ? new ArrayList<>() : (ArrayList<Object>) courseSet.getQualifiers();
                        objects.add(courseSubjectQualifier);
                        courseSet.setQualifiers(objects);
                    }
                    String courseNumber = prerequisiteElement.getNumber();
                    if (StringUtils.isNotEmpty(courseNumber)) {
                        List<Object> courseNumberQualifier = List.of("courseNumber", "=", courseNumber);
                        ArrayList<Object> objects = isNull(courseSet.getQualifiers()) ? new ArrayList<>() : (ArrayList<Object>) courseSet.getQualifiers();
                        objects.add(courseNumberQualifier);
                        courseSet.setQualifiers(objects);
                    }

                    if (StringUtils.isNotEmpty(courseAttrCode) && prerequisiteElement.getHidden()) {
                        coursesRequisite.setHidden(true);
                    }

                    courses.add(courseSet);
                    coursesRequisite.setCourses(courses);
                    requisites.add(coursesRequisite);
                    if (prerequisiteElement.getConcurrency() != null && prerequisiteElement.getConcurrency()) {
                        coursesRequisite.setConcurrency(prerequisiteElement.getConcurrency());
                    }

                    if (gradesMap.get(prerequisiteElement.getGrade()) != null) {
                        MinGradeSet minGradeSet = new MinGradeSet();
                        minGradeSet.setMinimumNumericalGrade(gradesMap.get(prerequisiteElement.getGrade()));
                        coursesRequisite.setMinGradeSet(minGradeSet);
                    }
                    if (nonNull(prerequisiteElement.getExclusions()) && !prerequisiteElement.getExclusions().isEmpty()) {
                        coursesRequisite.setExclusions(new ArrayList<>(prerequisiteElement.getExclusions().size()));
                        for (PrerequisiteExclusionModel prereq : prerequisiteElement.getExclusions()) {
                            coursesRequisite.getExclusions().add(courseExclusionConverter(prereq));
                        }
                    }
                    coursesRequisite.setNumberOfYears(prerequisiteElement.getNumberOfYears());
                    coursesRequisite.setFromTerm(prerequisiteElement.getFromTerm());
                    coursesRequisite.setToTerm(prerequisiteElement.getToTerm());
                    coursesRequisite.setCampCode(prerequisiteElement.getCampCode());
                    coursesRequisite.setCollCode(prerequisiteElement.getCollCode());
                    coursesRequisite.setDeptCode(prerequisiteElement.getDeptCode());
                    coursesRequisite.setComplCourses(prerequisiteElement.getComplCourses());
                    coursesRequisite.setComplCredits(prerequisiteElement.getComplCredits());
                    coursesRequisite.setMaxCourses(prerequisiteElement.getMaxCourses());
                    coursesRequisite.setMaxCoursesTransfer(prerequisiteElement.getMaxCoursesTransfer());
                    coursesRequisite.setMaxCredits(prerequisiteElement.getMaxCredits());
                    coursesRequisite.setMaxCreditsTransfer(prerequisiteElement.getMaxCreditsTransfer());
                    coursesRequisite.setMaxCredCrse(prerequisiteElement.getMaxCredCrse());
                    coursesRequisite.setMinCredCrse(prerequisiteElement.getMinCredCrse());
                    coursesRequisite.setReqCourses(prerequisiteElement.getReqCourses());
                    coursesRequisite.setReqCredits(prerequisiteElement.getReqCredits());
                    coursesRequisite.setMaxCredCond(prerequisiteElement.getMaxCredCond());
                    coursesRequisite.setReqCredCond(prerequisiteElement.getReqCredCond());
                    coursesRequisite.setReqCoursesCond(prerequisiteElement.getReqCoursesCond());
                    coursesRequisite.setConnectorReq(prerequisiteElement.getConnectorReq());
                    coursesRequisite.setConnectorMax(prerequisiteElement.getConnectorMax());
                    coursesRequisite.setConnectorTransfer(prerequisiteElement.getConnectorTransfer());
                }

                if (prerequisiteElement.getType().equals("student_group")) {
                    StudentGroupRequisite studentAttributeRequisite = new StudentGroupRequisite();
                    StudentGroupRequisiteItem group = new StudentGroupRequisiteItem();
                    group.setId(prerequisiteElement.getId());
                    List<StudentGroupRequisiteItem> list = new ArrayList<>();
                    list.add(group);
                    studentAttributeRequisite.setGroups(list);
                    studentAttributeRequisite.setOperator(prerequisiteElement.getOperator());
                    requisites.add(studentAttributeRequisite);
                }

                if (prerequisiteElement.getType().equals("placement")) {
                    PlacementRequisite placementRequisite = new PlacementRequisite();
                    List<PlacementRequisiteItem> placements = new ArrayList<>();

                    PlacementRequisiteItem placement = new PlacementRequisiteItem();
                    placement.setId(prerequisiteElement.getId());
                    placements.add(placement);
                    placementRequisite.setPlacements(placements);
                    requisites.add(placementRequisite);

                    List<ScoreRequisite> scoreRequisites = new ArrayList<>();
                    ScoreRequisite scoreRequisite = new ScoreRequisite();
                    scoreRequisite.setOperator(">=");

                    List<ScoreRequisiteItem> scores = new ArrayList<>();
                    ScoreRequisiteItem score = new ScoreRequisiteItem();
                    score.setValue(prerequisiteElement.getGrade());
                    scores.add(score);
                    scoreRequisite.setScores(scores);

                    scoreRequisites.add(scoreRequisite);

                    if (!StringUtils.isEmpty(prerequisiteElement.getMaxGrade())) {
                        ScoreRequisite maxScoreRequisite = new ScoreRequisite();
                        maxScoreRequisite.setOperator("<=");
                        scoreRequisites.add(maxScoreRequisite);
                        ScoreRequisiteItem maxScore = new ScoreRequisiteItem();
                        maxScore.setValue(prerequisiteElement.getMaxGrade());

                        maxScoreRequisite.setScores(Collections.singletonList(maxScore));
                    }

                    placementRequisite.setRequisites(scoreRequisites);
                }
            }

            if (requisite.getConnector() != null) {
                requisites.add(requisite);
            }
        }

        return requisites;
    }

    private void copyExtraFields(PrerequisiteDto prerequisites, Requisite requisite) {
        requisite.setAreaCode(prerequisites.getAreaCode());
        requisite.setRuleCode(prerequisites.getRuleCode());
        requisite.setGroupCode(prerequisites.getGroupCode());
        requisite.setRequiredNumberOfConditions(prerequisites.getRequiredNumberOfConditions());
        requisite.setConnectorReq(prerequisites.getConnectorReq());
        requisite.setConnectorCond(prerequisites.getConnectorCond());
        requisite.setConnectorMaxCond(prerequisites.getConnectorMaxCond());
        requisite.setConnectorMax(prerequisites.getConnectorMax());

        //
        requisite.setConnectorCond(prerequisites.getConnectorCond());
        requisite.setConnectorMaxCond(prerequisites.getConnectorMaxCond());
        requisite.setConnectorReq(prerequisites.getConnectorReq());
        requisite.setConnectorMax(prerequisites.getConnectorMax());
        requisite.setComplCourses(prerequisites.getComplCourses());
        requisite.setComplCredits(prerequisites.getComplCredits());
        requisite.setMaxCourses(prerequisites.getMaxCourses());
        requisite.setMaxCoursesTransfer(prerequisites.getMaxCoursesTransfer());
        requisite.setMaxCredits(prerequisites.getMaxCredits());
        requisite.setMaxCreditsTransfer(prerequisites.getMaxCreditsTransfer());
        requisite.setMaxCredCrse(prerequisites.getMaxCredCrse());
        requisite.setMinCredCrse(prerequisites.getMinCredCrse());
        requisite.setReqCourses(prerequisites.getReqCourses());
        requisite.setReqCredits(prerequisites.getReqCredits());
        requisite.setMaxCredCond(prerequisites.getMaxCredCond());
        requisite.setReqCoursesCond(prerequisites.getReqCoursesCond());
        requisite.setReqCredCond(prerequisites.getReqCredCond());
        requisite.setConnectorTransfer(prerequisites.getConnectorTransfer());
        requisite.setNumberOfYears(prerequisites.getNumberOfYears());
        requisite.setFromTerm(prerequisites.getFromTerm());
        requisite.setToTerm(prerequisites.getToTerm());
        requisite.setCampCode(prerequisites.getCampCode());
        requisite.setCollCode(prerequisites.getCollCode());
        requisite.setDeptCode(prerequisites.getDeptCode());
    }

    private CoursesRequisiteExclusion courseExclusionConverter(PrerequisiteExclusionModel model) {
        CoursesRequisiteExclusion coursesRequisiteExclusion = new CoursesRequisiteExclusion();
        coursesRequisiteExclusion.setStudentAttrCode(model.getStudentAttrCode());
        if (StringUtils.isNotEmpty(model.getCourseAttrCode())) {
            coursesRequisiteExclusion.setAttributes(Collections.singletonList(model.getCourseAttrCode()));
        }
        coursesRequisiteExclusion.setCampus(model.getCampus());
        coursesRequisiteExclusion.setCollege(model.getCollege());
        if (StringUtils.isNotEmpty(model.getDepartment())) {
            coursesRequisiteExclusion.setDepartments(
                    Collections.singletonList(new DepartmentDto(model.getDepartment(), null)));
        }
        coursesRequisiteExclusion.setTermStart(model.getTermStart());
        coursesRequisiteExclusion.setTermEnd(model.getTermEnd());
        coursesRequisiteExclusion.setId(model.getPrerequisitesCourseId());
        coursesRequisiteExclusion.setSubject(model.getPrerequisitesSubject());
        return coursesRequisiteExclusion;
    }

    //TODO: Simplify conditions
    private <T extends CoursePrerequisite> boolean prerequisiteCondition(T c) {
        return (nonNull(c.getPrerequisitesCourseId()) && !c.getPrerequisitesCourseId().trim().isEmpty() || nonNull(
                c.getTest()) && !c.getTest().trim().isEmpty() || nonNull(c.getLeftParen()) && !c.getLeftParen()
                .trim()
                .isEmpty() || nonNull(
                c.getRightParent()) && !c.getRightParent().trim().isEmpty() || nonNull(
                c.getPrerequisitesCourseId()) && !c.getPrerequisitesCourseId().trim().isEmpty());
    }

    public void addLevelCodes(Map<ScbcrseKey, CourseDto> baseCourses, List<ScrlevlKey> allScrlevl) {
        Map<CrseSimpleKey, List<ScrlevlKey>> scrlevlByCrse = allScrlevl.stream()
                .collect(groupingBy(it -> new CrseSimpleKey(it.getSubjCode(), it.getCrseNumb())));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (scrlevlByCrse.containsKey(crseSimpleKey)) {
                Set<String> levelCodes = scrlevlByCrse.get(crseSimpleKey)
                        .stream()
                        .sorted(Comparator.comparing(ScrlevlKey::getEffTerm, Comparator.reverseOrder()))
                        .filter(it -> map.getKey().getEffTerm().compareTo(it.getEffTerm()) >= 0)
                        .map(ScrlevlKey::getLevlCode)
                        .collect(Collectors.toSet());
                if (CollectionUtils.isNotEmpty(levelCodes)) {
                    map.getValue().setLevel(new ArrayList<>(levelCodes));
                }
            }
        }
    }

    public void addMutuallyExclusive(Map<ScbcrseKey, CourseDto> baseCourses, List<ScrmexcKey> allScrmexc) {
        Map<CrseSimpleKey, List<ScrmexcKey>> scrmexcByCrse = allScrmexc.stream()
                .collect(groupingBy(it -> new CrseSimpleKey(it.getSubjCode(), it.getCrseNumb())));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (scrmexcByCrse.containsKey(crseSimpleKey)) {
                Set<String> mutuallyExclusives = scrmexcByCrse.get(crseSimpleKey)
                        .stream()
                        .filter(it -> Objects.nonNull(it.getSubjCodeMexc()) && Objects.nonNull(it.getCrseNumbMexc()))
                        .sorted(Comparator.comparing(ScrmexcKey::getEffTerm, Comparator.reverseOrder()))
                        .filter(it -> map.getKey().getEffTerm().compareTo(it.getEffTerm()) >= 0)
                        .map(it -> String.format(ID_FORMATTER, it.getSubjCodeMexc(), it.getCrseNumbMexc()))
                        .collect(Collectors.toSet());
                if (CollectionUtils.isNotEmpty(mutuallyExclusives)) {
                    map.getValue().setMutuallyExclusive(new ArrayList<>(mutuallyExclusives));
                }
            }
        }
    }

    public void addEquivalents(Map<ScbcrseKey, CourseDto> baseCourses, List<ScreqivKey> allScreqiv) {
        Map<CrseSimpleKey, List<ScreqivKey>> screqivByCrse = allScreqiv.stream()
                .collect(groupingBy(it -> new CrseSimpleKey(it.getSubjCode(), it.getCrseNumb())));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (screqivByCrse.containsKey(crseSimpleKey)) {
                Set<String> equivalents = screqivByCrse.get(crseSimpleKey)
                        .stream()
                        .filter(it -> Objects.nonNull(it.getSubjCodeEqiv()) && Objects.nonNull(it.getCrseNumbEqiv()))
                        .sorted(Comparator.comparing(ScreqivKey::getEffTerm, Comparator.reverseOrder()))
                        .filter(it -> map.getKey().getEffTerm().compareTo(it.getEffTerm()) >= 0)
                        .map(it -> String.format(ID_FORMATTER, it.getSubjCodeEqiv(), it.getCrseNumbEqiv()))
                        .collect(Collectors.toSet());
                if (CollectionUtils.isNotEmpty(equivalents)) {
                    map.getValue().setEquivalents(new ArrayList<>(equivalents));
                }
            }
        }
    }

    public void addCorequisites(Map<ScbcrseKey, CourseDto> baseCourses, List<ScrcorqKey> allScrcorq) {
        Map<CrseSimpleKey, List<ScrcorqKey>> scrcorqByCrse = allScrcorq.stream()
                .collect(groupingBy(it -> new CrseSimpleKey(it.getSubjCode(), it.getCrseNumb())));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (scrcorqByCrse.containsKey(crseSimpleKey)) {
                Set<String> corequisites = scrcorqByCrse.get(crseSimpleKey)
                        .stream()
                        .filter(it -> Objects.nonNull(it.getSubjCodeCorq()) && Objects.nonNull(it.getCrseNumbCorq()))
                        .sorted(Comparator.comparing(ScrcorqKey::getEffTerm, Comparator.reverseOrder()))
                        .filter(it -> map.getKey().getEffTerm().compareTo(it.getEffTerm()) >= 0)
                        .map(it -> String.format(ID_FORMATTER, it.getSubjCodeCorq(), it.getCrseNumbCorq()))
                        .collect(Collectors.toSet());
                if (CollectionUtils.isNotEmpty(corequisites)) {
                    map.getValue().setCorequisites(new ArrayList<>(corequisites));
                }
            }
        }
    }

    public void addAttributes(Map<ScbcrseKey, CourseDto> baseCourses, List<ScrattrKey> allScrattr) {
        Map<CrseSimpleKey, List<ScrattrKey>> scrattrByCrse = allScrattr.stream()
                .collect(groupingBy(it -> new CrseSimpleKey(it.getSubjCode(), it.getCrseNumb())));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (scrattrByCrse.containsKey(crseSimpleKey)) {
                Set<String> attributes = scrattrByCrse.get(crseSimpleKey)
                        .stream()
                        .filter(it -> Objects.nonNull(it.getAttrCode()))
                        .sorted(Comparator.comparing(ScrattrKey::getEffTerm, Comparator.reverseOrder()))
                        .filter(it -> map.getKey().getEffTerm().compareTo(it.getEffTerm()) >= 0)
                        .map(ScrattrKey::getAttrCode)
                        .collect(Collectors.toSet());
                if (CollectionUtils.isNotEmpty(attributes)) {
                    map.getValue().setAttributes(new ArrayList<>(attributes));
                }
            }
        }
    }

    public void addYears(Map<ScbcrseKey, CourseDto> baseCourses, List<Map<String, Object>> allStvterm) {
        Map<String, Map<String, Object>> stvtermByCode = allStvterm.stream()
                .collect(toMap(it -> (String) it.get("STVTERM_CODE"), Function.identity()));

        baseCourses.entrySet().stream()
                .forEach(it -> it.getValue().setYear((String) stvtermByCode.get(it.getKey().getEffTerm()).get("STVTERM_ACYR_CODE")));
    }

    public void addTermDates(Map<ScbcrseKey, CourseDto> baseCourses, List<Map<String, Object>> allScbcrky) {
        Map<CrseSimpleKey, Map<String, Object>> scbcrkyByCrse = allScbcrky.stream()
                .collect(toMap(MapperFunctions.SCBCRKY_SIMPLE_CRSE_MAPPER, Function.identity()));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (scbcrkyByCrse.containsKey(crseSimpleKey)) {
                map.getValue().setTermStart((String) scbcrkyByCrse.get(crseSimpleKey).get("SCBCRKY_TERM_CODE_START"));
                map.getValue().setTermEnd((String) scbcrkyByCrse.get(crseSimpleKey).get("SCBCRKY_TERM_CODE_END"));
            }
        }
    }

    public void addDescriptions(Map<ScbcrseKey, CourseDto> baseCourses, List<Map<String, Object>> allScrtext) {
        Map<CrseSimpleKey, List<Map<String, Object>>> scrtextByCrse = allScrtext.stream()
                .collect(groupingBy(MapperFunctions.SCRTEXT_SIMPLE_CRSE_MAPPER));

        baseCourses.entrySet().stream()
                .filter(it -> scrtextByCrse.containsKey(new CrseSimpleKey(it.getKey().getSubjCode(), it.getKey().getCrseNumb())))
                .forEach(it -> it.getValue().setDescription(getDescription(it.getKey(), scrtextByCrse.get(new CrseSimpleKey(it.getKey().getSubjCode(), it.getKey().getCrseNumb())))));
    }

    public void addNarrativeDescriptions(Map<ScbcrseKey, CourseDto> baseCourses, List<Map<String, Object>> allScbdesc) {
        Map<CrseSimpleKey, List<Map<String, Object>>> scbdescByCrse = allScbdesc.stream()
                .collect(groupingBy(MapperFunctions.SCBDESC_SIMPLE_CRSE_MAPPER));

        for (Map.Entry<ScbcrseKey, CourseDto> map : baseCourses.entrySet()) {
            CrseSimpleKey crseSimpleKey = new CrseSimpleKey(map.getKey().getSubjCode(), map.getKey().getCrseNumb());
            if (scbdescByCrse.containsKey(crseSimpleKey)) {
                scbdescByCrse.get(crseSimpleKey)
                        .stream()
                        .sorted(Comparator.comparing(it -> (String) it.get("SCBDESC_TERM_CODE_EFF"), Comparator.reverseOrder()))
                        .filter(it -> checkTermInRange().test(map, it))
                        .findFirst()
                        .ifPresent(it -> map.getValue().setDescription((String) it.get("SCBDESC_TEXT_NARRATIVE")));
            }
        }
    }

    private BiPredicate<Map.Entry<ScbcrseKey, CourseDto>, Map<String, Object>> checkTermInRange() {
        return (courseMap, scbDescMap) -> {
            if (scbDescMap.get("SCBDESC_TERM_CODE_END") == null) {
                return courseMap.getKey().getEffTerm().compareTo((String) scbDescMap.get("SCBDESC_TERM_CODE_EFF")) >= 0;
            } else {
                return courseMap.getKey().getEffTerm().compareTo((String) scbDescMap.get("SCBDESC_TERM_CODE_EFF")) >= 0 &&
                        courseMap.getKey().getEffTerm().compareTo((String) scbDescMap.get("SCBDESC_TERM_CODE_END")) <= 0;
            }
        };
    }

    private String getDescription(ScbcrseKey scbcrseKey, List<Map<String, Object>> scrtextList) {
        Map<String, List<Map<String, Object>>> scrtextByEffTerm = scrtextList.stream()
                .collect(groupingBy(it -> (String) it.get("SCRTEXT_EFF_TERM")));
        Optional<String> effTerm = scrtextByEffTerm.keySet()
                .stream().sorted()
                .filter(it -> scbcrseKey.getEffTerm().compareTo(it) >= 0)
                .findFirst();

        return effTerm.map(s -> scrtextByEffTerm.get(s).stream()
                        .filter(it -> Objects.nonNull(it.get("SCRTEXT_SEQNO")))
                        .sorted(Comparator.comparingInt(map -> ((BigDecimal) map.get("SCRTEXT_SEQNO")).intValue()))
                        .map(it -> (String) it.get("SCRTEXT_TEXT"))
                        .collect(Collectors.joining()))
                .orElse(null);
    }

    public Map<ScbcrseKey, CourseDto> createBaseCourses(List<Map<String, Object>> scbcrse) {
        Map<ScbcrseKey, CourseDto> courses = new LinkedHashMap<>();
        for (Map<String, Object> baseCourse : scbcrse) {
            CourseDto courseDto = new CourseDto();
            courseDto.setId(String.format(ID_FORMATTER, baseCourse.get("SCBCRSE_SUBJ_CODE"), baseCourse.get("SCBCRSE_CRSE_NUMB")));
            courseDto.setCourseNumber((String) baseCourse.get("SCBCRSE_CRSE_NUMB"));
            courseDto.setSubject((String) baseCourse.get("SCBCRSE_SUBJ_CODE"));
            courseDto.setDisplayId(String.format(ID_FORMATTER, baseCourse.get("SCBCRSE_SUBJ_CODE"), baseCourse.get("SCBCRSE_CRSE_NUMB")));
            courseDto.setAggregatedId(String.format(ID_FORMATTER, baseCourse.get("SCBCRSE_SUBJ_CODE"), baseCourse.get("SCBCRSE_CRSE_NUMB")));
            courseDto.setEffectiveTerm((String) baseCourse.get("SCBCRSE_EFF_TERM"));
            courseDto.setCollege((String) baseCourse.get("SCBCRSE_COLL_CODE"));
            courseDto.setName((String) baseCourse.get("SCBCRSE_TITLE"));
            courseDto.setActive(getActive((String) baseCourse.get("SCBCRSE_CSTA_CODE")));
            courseDto.setMinCredits(getFloat((BigDecimal) baseCourse.get("SCBCRSE_CREDIT_HR_LOW")));
            courseDto.setMaxCredits(getFloat((BigDecimal) baseCourse.get("SCBCRSE_CREDIT_HR_HIGH")));
            courseDto.setCreditRangeMode((String) baseCourse.get("SCBCRSE_CREDIT_HR_IND"));
            courseDto.setRepeatLimit(getInteger((BigDecimal) baseCourse.get("SCBCRSE_REPEAT_LIMIT")));
            courseDto.setMaximumRepeatCredits(getInteger((BigDecimal) baseCourse.get("SCBCRSE_MAX_RPT_UNITS")));

            // <#if courseConfiguration.includeCreditStatusCode>
            String creditStatusCode = (String) baseCourse.get("SCBCRSE_CREDIT_STATUS_CODE");
            if (creditStatusCode != null) {
                courseDto.setNoncredit(creditStatusCode.equals("N"));
                courseDto.setDegreeApplicable(creditStatusCode.equals("D"));
                courseDto.setCreditStatus(creditStatusCode);
            }

            String department = (String) baseCourse.get("SCBCRSE_DEPT_CODE");
            if (department != null) {
                courseDto.setDepartments(List.of(new DepartmentDto(department, null)));
            }
            courses.put(SCBCRSE_KEY_MAPPER.apply(baseCourse), courseDto);
        }
        return courses;
    }

    private boolean getActive(String active) {
        return "A".equalsIgnoreCase(active);
    }
}
